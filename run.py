import parcas as pca
import config as cfg
import numpy as np
import cv2
import os

ff = pca.avrg_field_from_folder(cfg.PROJECT_PATH + "ff/")
np.save(cfg.FF_PATH, ff)
df = pca.avrg_field_from_folder(cfg.PROJECT_PATH + "df/")
np.save(cfg.DF_PATH, df)
ff = pca.read_img(cfg.FF_PATH)
df = pca.read_img(cfg.DF_PATH)

stack = pca.Stack(cfg.RGB_PATH, cfg.FFC_PATH, cfg.DIM, ff=ff, df=df)
stack = pca.build_stack(stack, cfg.THRESH_RGB, cfg.DIM, start_pos=cfg.START_POS, min_area=cfg.MIN_AREA, ovd=True)

mosaic = pca.Mosaic(cfg.DIM, stack=stack, start_pos=cfg.START_POS)

mosaic = pca.find_particles(mosaic, cfg.THRESH_FFC, key="ffc", dil_iterations=cfg.DIL_ITERATIONS, min_area=cfg.MIN_AREA,
                            marked_path=cfg.MARKED_PATH)

if cfg.XML_PATH:
    print("Writing " + cfg.XML_PATH)
    pca.mosaic_to_xml(mosaic, cfg.XML_PATH)


