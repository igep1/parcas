from cv2 import imread
from numpy import load, ndarray
import pandas as pd
__all__ = ["Particle"]


class Particle:
    def __init__(self, label=None, path=None, rpos=(0, 0), pos=(0, 0), mpos=(0, 0), static_shape=None, touches=None,
                 luminance=0, area=0, mass_lum=(0, 0), mass_area=(0, 0), gyrad=0, d2=(0, 0), d3=(0, 0),
                 belongs_to_group=False):
        self.label = label
        if path:
            assert isinstance(path, dict)
            self.path = path
        else:
            self.path = dict()
        self.pos = pos
        self.rpos = rpos
        self.mpos = mpos
        self.static_shape = static_shape
        self.belongs_to_group = belongs_to_group
        if touches:
            assert isinstance(touches, dict)
            self.touches = touches
        else:
            self.touches = dict()
        self.luminance = luminance
        self.area = area
        self.mass_lum = mass_lum
        self.mass_area = mass_area
        self.gyrad = gyrad
        self.d2 = d2
        self.d3 = d3
        if not static_shape:
            self.update_static_shape()
        else:
            self.static_shape = static_shape

    def __repr__(self):
        return "<Slice mpos:% label:%s touches:%s path:%s rpos:%s pos:%s static_shape:%s luminance:%s mass_lum:%s" \
               "mass_area:%s gyrad:%s d2:%s d3:%s>" %\
               (self.mpos, self.label, self.rpos, self.pos, self.static_shape,
                self.luminance, self.mass_lum, self.mass_area, self.gyrad, self.d2, self.d3, self.touches, self.path)

    def __eq__(self, other):
        return self.mpos == other.mpos and self.label == other.label

    def collect_data(self):
        return pd.DataFrame(data={"row": self.mpos[0],
                                  "column": self.mpos[1],
                                  "label": self.label,
                                  "luminance": self.luminance,
                                  "area": self.area,
                                  "mass_lum": self.mass_lum[0],
                                  "mass_lum_err": self.mass_lum[1],
                                  "mass_area": self.mass_area[0],
                                  "mass_area_err": self.mass_area[1],
                                  "d2": self.d2[0],
                                  "d2_err": self.d2[1],
                                  "d3": self.d3[0],
                                  "d3_err": self.d3[1],
                                  "rpos_x": self.rpos[0],
                                  "rpos_y": self.rpos[1],
                                  "pos_x": self.pos[0],
                                  "pos_y": self.pos[1],
                                  "height": self.static_shape[0],
                                  "width": self.static_shape[1]},
                            index=[0])

    def read_image(self, key, flags=-1):
        if self.path:
            if self.path[key].split(".")[-1] == "npy":
                return load(self.path[key])
            else:
                return imread(self.path[key], flags=flags)

    def update_static_shape(self, key=None):
        if not key:
            key = list(self.path.keys())[0]
        img = self.read_image(key, flags=0)
        if img:
            self.static_shape = img.shape[:2]

    def update_touches(self, slc, to_index=4):
        self.touches["mosaic_edge"] = self.touches_mosaic_edge(slc)
        self.touches["image_edge"] = self.touches_image_edge(slc)
        self.touches["overlap"] = self.is_in_overlap(slc, to_index)
        self.touches["overlap_edge"] = self.touches_overlap_edge(slc, to_index)

    def update_mass(self, mono_luminance, mono_area):
        mass_lum = self.luminance/mono_luminance[0]
        mass_lum_err = mass_lum * mono_luminance[1]/mono_luminance[0]
        mass_area = self.area/mono_area[0]
        mass_area_err = mass_area * mono_area[1]/mono_area[0]
        self.mass_lum = (mass_lum, mass_lum_err)
        self.mass_area = (mass_area, mass_area_err)

    def is_in_overlap(self, slc, to_index=4):
        indices = []
        for ind, overlap in enumerate(slc.overlaps[:to_index]):
            if overlap:
                if overlap[0][0] <= self.rpos[0] and self.rpos[0] + self.static_shape[1] - 1 <= overlap[1][0]\
                        and overlap[0][1] <= self.rpos[1] and self.rpos[1] + self.static_shape[0] - 1 <= overlap[1][1]:
                    indices.append(ind)
        return indices

    def touches_mosaic_edge(self, slc):
        for e, edge in enumerate(slc.edges):
            if edge:
                if e < 2 and self.rpos[0] == e * (slc.static_shape[1] - 1):
                    if edge[0] < self.rpos[1] < edge[1]:
                        return e
                elif self.rpos[1] == (e - 2) * (slc.static_shape[0] - 1):
                    if edge[0] < self.rpos[0] < edge[1]:
                        return e
        return -1

    def touches_image_edge(self, slc):
        if type(slc) == ndarray:
            shp = slc.shape
        else:
            shp = slc.static_shape
        if self.rpos[0] == 0:
            return 0
        elif self.rpos[0] + self.static_shape[1] == shp[1]:
            return 1
        elif self.rpos[1] == 0:
            return 2
        elif self.rpos[1] + self.static_shape[0] == shp[0]:
            return 3
        else:
            return -1

    def touches_overlap_edge(self, slc, to_index=4):
        indices = []
        for ind, overlap in enumerate(slc.overlaps[:to_index]):
            added = False
            if overlap:
                for j in (0, 1):
                    for i in (0, 1):
                        if not overlap[j][i] == j * (slc.static_shape[1-i] - 1):
                            if self.rpos[i] <= overlap[j][i] < self.rpos[i] + self.static_shape[1-i] \
                                    and overlap[j][1-i] - self.static_shape[i] < self.rpos[1-i] <= overlap[1-j][1-i]:
                                indices.append(ind)
                                added = True
                                break
                    if added:
                        break
        return indices








