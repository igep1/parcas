import cv2
import numpy as np

__all__ = ["cvshow", "OverlapDisplay"]


class OverlapDisplay:
    def __init__(self, img_shape, size=0.1):
        self.size = size
        self.img_shape = img_shape
        self.canvas = np.zeros((3 * img_shape[0], 3 * img_shape[1]), dtype=np.uint8)

    def clear(self):
        self.canvas = np.zeros((3 * self.img_shape[0], 3 * self.img_shape[1]), dtype=np.uint8)

    def insert(self, img1, img2, rpos):
        self.canvas[img1.shape[0]:2 * img1.shape[0], img1.shape[1]:2 * img1.shape[1]] = img1
        self.canvas[img2.shape[0] + rpos[1]: 2 * img2.shape[0] + rpos[1],
                    img2.shape[1] + rpos[0]: 2 * img2.shape[1] + rpos[0]] = img2

    def insert_slices(self, slc1, slc2):
        img1 = slc1.export()
        img2 = slc2.export()
        rpos = img2.rpos
        self.insert(img1, img2, rpos)

    def outline(self, rpos, thickness=12):
        cv2.rectangle(self.canvas,
                      (self.img_shape[1], self.img_shape[0]),
                      (2 * self.img_shape[1], 2 * self.img_shape[0]),
                      100, thickness=thickness)
        cv2.rectangle(self.canvas,
                      (self.img_shape[1] + rpos[0], self.img_shape[0] + rpos[1]),
                      (2 * self.img_shape[1] + rpos[0], 2 * self.img_shape[0] + rpos[1]),
                      100, thickness=thickness)

    def draw_indices(self, f, thickness=20):
        txt, baseline = cv2.getTextSize(str(f + 1) + " + " + str(f), cv2.FONT_HERSHEY_PLAIN, 20, thickness=thickness)
        cv2.putText(self.canvas, str(f + 1) + " + " + str(f), (50, 50 + txt[1]), cv2.FONT_HERSHEY_PLAIN, 20, 255,
                    thickness=thickness)

    def show(self):
        cvshow(self.canvas, self.size, "Overlap Display", t=1)

    def next(self, a, b, index, rpos=None):
        if not isinstance(a, np.ndarray):
            img1 = a.export()
        else:
            img1 = a
        if not isinstance(b, np.ndarray):
            img2 = b.export()
        else:
            img2 = b
        if not rpos:
            rpos = b.rpos
        else:
            rpos = rpos
        self.clear()
        self.insert(img1, img2, rpos)
        self.outline(rpos)
        self.draw_indices(index)


def cvshow(img, size=1., name="Image", t=0, pos=(0, 0)):
    s = img.shape
    resize_img = cv2.resize(img, dsize=(int(s[1] * size), int(s[0] * size)))
    cv2.namedWindow(name)
    cv2.moveWindow(name, pos[0], pos[1])
    cv2.imshow(name, resize_img)
    cv2.waitKey(t)



