TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT, BOTTOM_RIGHT = 0, 1, 2, 3
MODE_NORMAL, MODE_ADAPTIVE = 0, 1
LEFT, RIGHT, UP, DOWN = 0, 1, 2, 3
COLOR_BW, COLOR_GRAY, COLOR_RGB = 0, 1, 2
NEIGHBOR_INDICES = [(0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0), (-1, 1)]

__all__ = []
__all__ += ["TOP_LEFT", "TOP_RIGHT", "BOTTOM_LEFT", "BOTTOM_RIGHT"]
__all__ += ["MODE_NORMAL", "MODE_ADAPTIVE"]
__all__ += ["LEFT", "RIGHT", "UP", "DOWN"]
__all__ += ["COLOR_BW", "COLOR_GRAY",  "COLOR_RGB"]
__all__ += ["NEIGHBOR_INDICES"]

