from .Mosaic.Slice import Slice

__all__ = ["contains_any", "appropriate_z"]


def contains_any(iter1, iter2):
    for item in iter2:
        if item in iter1:
            return True
    return False


def appropriate_z(slc, z=None):
    if type(slc) == Slice:
        return 0
    else:
        if not z or z < 0:
            return slc.monomer_plane
        else:
            return z


