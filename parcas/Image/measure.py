import numpy as np
from scipy.optimize import curve_fit
import cv2
from .process import cvt_bgr_to_gray

__all__ = ["measure_luminance", "boxcount", "get_gyrad"]


def measure_luminance(img):
    if len(img.shape) == 3:
        if img.dtype == np.float32:
            img = cvt_bgr_to_gray(img)
        else:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return np.sum(img)


def boxcount(image):
    b = 5  # initial box size in pixels
    f = 1.1  # box scaling factor
    n = 20  # number of graph points for simple linear regression
    gx = []  # x coordinates of graph points
    gy = []  # y coordinates of graph points
    img_h, img_w = image.shape
    for ib in range(n):
        bs = int(round(b * f ** ib))  # box size in pixels
        if bs > img_w or bs > img_h:
            break
        bnx = int(img_w / bs)  # of boxes in x direction of image
        bny = int(img_h / bs)  # of boxes in y direction of image
        box_count = 0
        for by in range(bny):
            for bx in range(bnx):
                # if there are any pixels in the box then increase box count
                found_pixel = False
                for ky in range(bs):
                    for kx in range(bs):
                        if image[bs * by + ky, bs * bx + kx] == 255:
                            found_pixel = True
                            box_count += 1
                            break
                    if found_pixel:
                        break
        if box_count != 0:
            gx.append(1.0 / bs)
            gy.append(box_count)

    def fit(x, m, b):
        return m * np.log(x) + b

    popt, pcov = curve_fit(fit, gx, np.log(gy))
    stdev = np.sqrt(np.diag(pcov))

    return popt[0], stdev[0]


def get_gyrad(image):
    # get centroid
    M = cv2.moments(image)
    cx = int(round(M["m10"] / M["m00"]))
    cy = int(round(M["m01"] / M["m00"]))

    # get all pixel except centroid
    pixels = np.transpose(np.nonzero(image))
    pixels = np.delete(pixels, np.where(pixels == (cy, cx)), 0)

    # get distances to centroid
    N = len(pixels)
    dist = np.zeros(N)
    for i, p in enumerate(pixels):
        dist[i] = (p[1] - cx) ** 2 + (p[0] - cy) ** 2
    dist = np.sort(dist)

    # print(np.sqrt(1 / N * sum([dist[i] for i in range(N)])))
    # gyrad = np.sqrt(1 / N * sum([dist[i] for i in range(N)])) - np.sqrt(monomer_area/math.pi)
    gyrad = np.sqrt(1 / N * sum([dist[i] for i in range(N)]))

    return gyrad