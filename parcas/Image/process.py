import cv2
import numpy as np
from os import listdir
from ..display import cvshow
from .. import utils

__all__ = ["fill_holes", "fill_porous", "fill_1px_gaps", "ffc", "apply_label_mask", "norm_to_background",
           "avrg_field_from_folder", "draw_labelled_box", "cvt_bgr_to_gray", "clear_single_pixels"]


def fill_holes(img):
    img = cv2.copyMakeBorder(img, 1, 1, 1, 1, cv2.BORDER_CONSTANT, value=0)
    copy = np.copy(img)
    ch, cw = copy.shape
    mask = np.zeros((ch+2, cw+2), np.uint8)
    cv2.floodFill(copy, mask, (0, 0), 255)
    copy = cv2.bitwise_not(copy)
    img = cv2.bitwise_or(img, copy)
    img = img[1:ch-1, 1:cw-1]
    return img


def fill_porous(img, erode=10, border=True):
    copy = np.copy(img)
    # show(copy, 4, t=0)
    # copy = cv2.copyMakeBorder(copy, 1, 1, 1, 1, cv2.BORDER_CONSTANT, value=255)
    ch, cw = copy.shape
    if border:
        copy = cv2.rectangle(copy, (0, 0), (cw, ch), 255, thickness=1)

    # show(copy, 4)
    for p in range(cw):
        if copy[1, p] == 0:
            m = np.zeros((ch + 2, cw + 2), np.uint8)
            cv2.floodFill(copy, m, (p, 1), 255)
            break

    copy = cv2.bitwise_not(copy)
    # copy = copy[1:ch - 1, 1:cw - 1]
    # show(copy, 4, t=0)

    k = np.ones((2, 2), np.uint8)
    er_copy = cv2.erode(copy, k, iterations=erode)

    ch, cw = copy.shape
    holes = np.transpose(np.nonzero(er_copy))
    for hole in holes:
        hole = (hole[1], hole[0])
        if copy[hole[1], hole[0]] != 0:
            m = np.zeros((ch + 2, cw + 2), np.uint8)
            cv2.floodFill(copy, m, hole, 0)
            break
    # show(copy, 4, t=0)

    img = img | copy

    for x in range(cw):
        if x != 0:
            if img[0, x-1] == 255 and img[1, x] == 255:
                img[0, x] = 255
            if img[ch-1, x-1] == 255 and img[ch-2, x] == 255:
                img[ch-1, x] = 255
        else:
            if img[1, x] == 255:
                img[0, x] = 255
            if img[ch-2, x] == 255:
                img[ch-1, x] = 255
    for y in range(ch):
        if y != 0:
            if img[y-1, 0] == 255 and img[y, 1] == 255:
                img[y, 0] = 255
            if img[y-1, cw-1] == 255 and img[y, cw-2] == 255:
                img[y, cw-1] = 255
        else:
            if img[y, 1] == 255:
                img[y, 0] = 255
            if img[y, cw-2] == 255:
                img[y, cw-1] = 255

    return img


def fill_1px_gaps(img):
    shape = img.shape
    img_h = shape[0]
    img_w = shape[1]
    _, _, stats, _ = cv2.connectedComponentsWithStats(img, 4, cv2.CV_32S)
    stats = stats[np.lexsort(stats.T[[4]])]
    stats = stats[:-1]
    for s in stats:
        if s[4] > 10:
            x = s[0]
            y = s[1]
            w = s[2]
            h = s[3]
            for j in range(y, y+h):
                for i in range(x, x+w):
                    if img[j][i] == 0 and j not in (0, img_h-1) and i not in (0, img_w-1):
                        if (img[j][i] != img[j][i + 1] and img[j][i] != img[j][i - 1]) \
                                or (img[j][i] != img[j + 1][i] and img[j][i] != img[j - 1][i]):
                            img[j][i] = 255
    return img


def cvt_bgr_to_gray(img):
    B = img[:, :, 0]
    G = img[:, :, 1]
    R = img[:, :, 2]
    return np.array(B * 0.114 + G * 0.587 + R * 0.299)


def ffc(img, ff, df, gray=True):
    gain = np.float32(np.mean(ff - df)) / (ff - df)
    img = (img - df) * gain
    if gray and len(img.shape) == 3:
        img = cvt_bgr_to_gray(img)
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    return img


def apply_label_mask(img, labels, part):
    mask = labels[part.rpos[1]:part.rpos[1] + part.static_shape[0],
                  part.rpos[0]:part.rpos[0] + part.static_shape[1]]
    mask[mask != part.label] = 0
    mask[mask == part.label] = 255
    mask = mask.astype(np.uint8)
    mask = fill_holes(mask)
    mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
    img_part = img[part.rpos[1]:part.rpos[1] + part.static_shape[0],
                   part.rpos[0]:part.rpos[0] + part.static_shape[1]]
    if len(img_part.shape) == 2:
        mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY)
    img_masked = mask - img_part
    img_masked = mask - img_masked
    return img_masked


def norm_to_background(img, ff, df):
    # bg = ffc(ff, ff, df)
    gain = np.float32(np.mean(ff - df)) / (ff - df)
    bg = img/gain + df
    # bg = ff-df
    img = img / bg
    print(np.mean(bg))
    np.set_printoptions(threshold=1000)
    # bg = np.around(bg).astype(np.uint8)
    # bg = cv2.cvtColor(bg, cv2.COLOR_BGR2GRAY)
    print(bg)
    cvshow(img, 0.3, "ff-df", t=0)


def clear_single_pixels(img):
    img = cv2.erode(img, np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], np.uint8))
    img = cv2.dilate(img, np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], np.uint8))
    return img


def avrg_field_from_folder(path):
    fields = listdir(path)
    if fields:
        res = np.zeros(cv2.imread(path + fields[0]).shape, dtype=np.float32)
        for field in fields:
            res += cv2.imread(path + field)
        return res / len(fields)


def draw_labelled_box(img, part, color=(255, 255, 255), font_face=cv2.FONT_HERSHEY_PLAIN, font_scale=1, thickness=1):
    img = cv2.rectangle(img, part.rpos,
                        (part.rpos[0] + part.static_shape[1] - 1, part.rpos[1] + part.static_shape[0] - 1),
                        color, thickness)

    text_size, _ = cv2.getTextSize(str(part.label), font_face, font_scale, thickness)
    text_x, text_y = part.rpos[0], part.rpos[1] - 1
    if part.rpos[1] - text_size[1] - 1 <= 0:
        text_y = part.rpos[1] + part.static_shape[0] + text_size[1] + 1
    if part.rpos[0] + text_size[0] >= img.shape[1]:
        text_x = img.shape[1] - text_size[0]

    cv2.putText(img, str(part.label), (text_x, text_y), font_face, font_scale, color, thickness)

    return img


def write_marked_img(slc, out_path, img=None, key=None, z=None):
    if not type(img) == np.ndarray:
        img = slc.read_image(key, utils.appropriate_z(slc, z), flags=1)
    else:
        if len(img.shape) < 3:
            img = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)

    colors = dict(
        dark_red=(0, 0, 128),
        red=(0, 0, 255),
        dark_green=(0, 128, 0),
        green=(0, 255, 0),
        dark_blue=(128, 0, 0),
        blue=(255, 0, 0),
        light_blue=(255, 255, 0),
        yellow=(0, 255, 255),
        pink=(255, 0, 255),
        purple=(255, 0, 128)
    )

    h, w = slc.static_shape[:2]
    h -= 1
    w -= 1
    for e, edge in enumerate(slc.edges):
        if edge:
            if e < 2:
                img = cv2.line(img, (e * w, edge[0]), (e * w, edge[1]), colors["red"], thickness=1)
            else:
                img = cv2.line(img, (edge[0], (e - 2) * h), (edge[1], (e - 2) * h), colors["red"], thickness=1)
    for overlap in slc.overlaps[4:]:
        if overlap:
            img = cv2.rectangle(img, overlap[0], overlap[1], colors["blue"], thickness=1)
    for overlap in slc.overlaps[:4]:
        if overlap:
            img = cv2.rectangle(img, overlap[0], overlap[1], colors["green"], thickness=1)

    # particles to disregard
    for part in slc.parts["small"]:
        img = draw_labelled_box(img, part, colors["yellow"])
    for part in slc.parts["unknown"]:
        img = draw_labelled_box(img, part, colors["red"])
    for part in slc.parts["unnecessary"]:
        img = draw_labelled_box(img, part, colors["blue"])
    for part in slc.parts["overlap"]:
        img = draw_labelled_box(img, part, colors["green"])
    # particles to analyze
    for part in slc.parts["full"]:
        img = draw_labelled_box(img, part, colors["pink"])
    for part in slc.parts["segment"]:
        img = draw_labelled_box(img, part, colors["light_blue"])

    cv2.imwrite(out_path, img)

