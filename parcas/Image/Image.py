import cv2
import numpy as np

#############################################
# DEPRECIATED
#############################################

# __all__ = ["new_from_path", "new_from_array"]
__all__ = ["Image"]


class Image(np.ndarray):
    def __new__(cls, shape=(0, 0), dtype=np.uint8, buffer=None, offset=0, strides=None, order=None,
                arr=None, path="", rpos=(0, 0), pos=(0, 0), static_shape=(0, 0), con=None, roi=None, read=False):
        if arr:
            obj = np.asarray(arr).view(cls)
        else:
            obj = super(Image, cls).__new__(cls, shape, dtype, buffer, offset, strides, order)
        obj.path = path
        obj.rpos = rpos
        obj.pos = pos
        obj.con = con
        obj.roi = roi
        if read:
            obj.read()
        if static_shape == (0, 0):
            if read:
                obj.static_shape = obj.shape[:2]
            else:
                obj.read(flags=0)
                obj.static_shape = obj.shape[:2]
                obj.clear()
        else:
            obj.static_shape = static_shape
        return obj

    def __repr__(self):
        return "<Image %s path:%s rpos:%s pos:%s static_shape:%s>" % (self, "../" + self.path.split("/")[-1],
                                                                      self.rpos, self.pos, self.static_shape)

    def __array_finalize__(self, obj):
        if obj is None:
            return
        self.path = getattr(obj, "path", "")
        self.pos_rel = getattr(obj, "pos_rel", (0, 0))
        self.pos_real = getattr(obj, "pos_real", (0, 0))
        self.con = getattr(obj, "con", [])
        self.roi = getattr(obj, "roi", None)
        self.static_shape = getattr(obj, "static_shape", (0, 0))

    def read(self, flags=-1):
        img = cv2.imread(self.path, flags=flags)
        self.resize(img.shape, refcheck=False)
        self.shape = img.shape
        self[::] = img

    def export(self, flags=-1):
        return cv2.imread(self.path, flags=flags)

    def clear(self):
        self.resize((0, 0), refcheck=False)


# def new_from_path(path, rpos=(0, 0), pos=(0, 0), static_shape=(0, 0), con=None, roi=None, read=False):
#     return Image(path=path, rpos=rpos, pos=pos, static_shape=static_shape, con=con, roi=roi, read=read)
#
#
# def new_from_array(arr, rpos=(0, 0), pos=(0, 0), static_shape=(0, 0), con=None, roi=None, read=False):
#     return Image(arr=arr, rpos=rpos, pos=pos, static_shape=static_shape, con=con, roi=roi, read=read)

