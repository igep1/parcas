from . import constants as const
from . import display
from . import utils
from . import io
from .Mosaic.Slice import Slice
from .Image import compare, process
import numpy as np
import cv2
from time import sleep
import os
import sys

__all__ = ["build_stack", "find_particles"]


def build_stack(stack, thresh, dim, key="ffc", z=None, start_pos=const.TOP_LEFT, min_area=50, min_edge_dist=250,
                roi_max_part=20, roi_fail_count=(10, 10), roi_start_count=2, roi_unique=None, ovd=False):
    """
    Calculate relative position of each Slice in the Stack
    :param stack: Stack of Slices
    :param thresh: threshold used to find particles
    :param dim: mosaic dimensions
    :param key: key to image folder
    :param z: height index for ZSlices, standard value: monomer_plane, ignored for Slices
    :param start_pos: starting corner position
    :param ovd: whether an Overlap Display should be shown
    :param min_area: minimum particle area
    :param min_edge_dist: minimum distance from edge to be considered in roi
    :param roi_max_part: max monomer count to be considered in roi (multiple of min_area)
    :param roi_fail_count: amount of attempts when comparing rois
    :param roi_start_count: initial number of particles in roi
    :param roi_unique: dict with unique settings for roi params for given index (key = i<index>)
    :return: Stack with accurate relative positions
    """
    if ovd:
        shape = stack[0].static_shape[:2]
        ovd = display.OverlapDisplay(shape)
    else:
        ovd = None

    z0 = utils.appropriate_z(stack[0], z)
    img1 = stack[0].read_image(z=z0, key=key)

    img1_bin = np.around(img1).astype(np.uint8)
    _, img1_bin = cv2.threshold(img1_bin, thresh, 255, cv2.THRESH_BINARY_INV)
    img1_bin = process.clear_single_pixels(img1_bin)

    d = 0
    if start_pos == const.TOP_LEFT:
        d = 1
    elif start_pos == const.TOP_RIGHT:
        d = -1

    for f in range(1, len(stack)):
        z0 = utils.appropriate_z(stack[f], z)
        img2 = stack[f].read_image(z=z0, key=key)

        img2_bin = np.around(img2).astype(np.uint8)
        _, img2_bin = cv2.threshold(img2_bin, thresh, 255, cv2.THRESH_BINARY_INV)
        img2_bin = process.clear_single_pixels(img2_bin)

        _, labels, stats, centroids = cv2.connectedComponentsWithStats(img2_bin, 4, cv2.CV_32S)
        stats = stats[np.lexsort(stats.T[[4]])]
        stats = stats[:-1]

        #############################################
        # Get Relative Coordinates
        #############################################
        rpos = None
        rev = False
        n = roi_start_count
        fail_count = roi_fail_count
        max_part = roi_max_part
        if roi_unique:
            if "i{}".format(f) in list(roi_unique.keys()):
                unique = roi_unique["i{}".format(f)]
                n = unique[0]
                fail_count = unique[1]
                max_part = unique[2]

        n -= 1
        roi1, roi2 = [], []
        while rpos is None and n < fail_count[1]:
            n += 1
            if f % dim[0] != 0:
                if d == -1:
                    roi1, roi2 = compare.get_rois(img2, stats, const.RIGHT, min_area, min_edge_dist, n,
                                                  max_part=max_part, f=f, draw_rois=True)
                else:
                    roi1, roi2 = compare.get_rois(img2, stats, const.LEFT, min_area, min_edge_dist, n,
                                                  max_part=max_part, f=f, draw_rois=True)
            else:
                roi1, roi2 = compare.get_rois(img2, stats, const.UP, min_area, min_edge_dist, n,
                                              max_part=max_part, f=f, draw_rois=True)
                rev = True
            rpos = compare.get_rpos(img1, roi1, roi2, fail_count[0], draw_rois=True if f ==1 else False)
        if rev:
            d *= -1
        if n == fail_count[1]:
            if type(stack[f] == Slice):
                print("\nTemplate Search Unsuccessful for Image " + stack[f].path[key] + " (" + str(f + 1) + ").")
            else:
                print("\nTemplate Search Unsuccessful for Image " + stack[f].path[key][z0] + " (" + str(f + 1) + ").")
            sys.exit()
        # display.cvshow(roi1[4].astype(np.uint8), size=0.3)
        # display.cvshow(roi2[4].astype(np.uint8), size=0.3)
        stack[f].roi = (roi1, roi2)
        stack[f].rpos = rpos

        if ovd:
            ovd.next(img1, img2, f, rpos=rpos)
            ovd.show()
        else:
            print(f)

        img1 = img2

    if ovd:
        sleep(1)
        cv2.destroyAllWindows()

    return stack


def find_particles(mosaic, thresh, dil_iterations=-1, dil_kernel=None, key="ffc", z=None, min_area=50,
                   marked_path=None, rename_to_mosaic_index=True):
    """
    Find particles in mosaic and sort them into groups
    :param mosaic: Mosaic
    :param thresh: threshold used to find particles
    :param dil_iterations: number of times to dilate the bw image before finding particles
    :param dil_kernel: dilation kernel
    :param key: key to image folder
    :param z: height index for ZSlices, standard value: monomer_plane, ignored for Slices
    :param min_area: minimum particle area
    :param marked_path: if given, writes images with marked and labelled particles to marked_path
    :param rename_to_mosaic_index: whether to rename all images to include mosaic position in their name
    :return: mosaic with all found particles
    """
    if not dil_kernel:
        dil_kernel = np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], np.uint8)
    if marked_path:
        if not os.path.exists(marked_path):
            os.makedirs(marked_path)

    for j in range(mosaic.dim[1]):
        for i in range(mosaic.dim[0]):
            z0 = utils.appropriate_z(mosaic[j][i], z)
            img = mosaic[j][i].read_image(key=key, z=z0)

            if rename_to_mosaic_index:
                org_path = mosaic[j][i].path[key]
                if type(org_path) == list:
                    for k in range(len(org_path)):
                        new_path = org_path[k].split("/")
                        frmt = new_path[-1].split(".")[-1]
                        new_path[-1] = str(j) + "_" + str(i) + "_" + str(k) + "." + frmt
                        new_path = "/".join(new_path)
                        os.rename(org_path[k], new_path)
                        mosaic[j][i].path[key][k] = new_path
                else:
                    new_path = org_path.split("/")
                    frmt = new_path[-1].split(".")[-1]
                    new_path[-1] = str(j) + "_" + str(i) + "." + frmt
                    new_path = "/".join(new_path)
                    os.rename(org_path, new_path)
                    mosaic[j][i].path[key] = new_path

            img_bin = np.around(img).astype(np.uint8)
            if len(img.shape) == 3:
                img_bin = cv2.cvtColor(img_bin, cv2.COLOR_BGR2GRAY)
            _, img_bin = cv2.threshold(img_bin, thresh, 255, cv2.THRESH_BINARY_INV)
            img_bin = process.clear_single_pixels(img_bin)
            if dil_iterations > 0:
                img_bin = cv2.dilate(img_bin, dil_kernel, iterations=dil_iterations)
            # display.cvshow(img_bin, size=0.3, t=1)

            _, labels, stats, _ = cv2.connectedComponentsWithStats(img_bin, 8, cv2.CV_32S)
            del img_bin
            stats = stats[1:]

            mosaic[j][i] = compare.sort_particles(stats, mosaic[j][i], min_area=min_area)

            if marked_path:
                process.write_marked_img(mosaic[j][i], marked_path + str(j) + "_" + str(i) + ".png", img=img)
            del img

    #############################################
    # Stitch Segmented Particles (untested, extremely rare case)
    #############################################
    stitch = False
    if stitch:
        def find_segments(seg_root, mosaic, group):
            for o in seg_root.touches["overlap"]:
                idx = seg_root.mpos + const.NEIGHBOR_INDICES[o]
                for seg_branch in mosaic[idx[0]][idx[1]].parts["segment"]:
                    if o + 4 in seg_branch.touches["overlap"]:
                        if not seg_branch.belongs_to_group:
                            if compare.get_overlap(seg_root, seg_branch):
                                seg_branch.belongs_to_group = True
                                group.append(seg_branch)
                                group = find_segments(seg_branch, mosaic, group)
            return group
        groups = []
        for j in range(mosaic.dim[1]):
            for i in range(mosaic.dim[0]):
                for seg_root in mosaic[j][i].parts["segment"]:
                    if not seg_root.belongs_to_group:
                        seg_root.belongs_to_group = True
                        groups.append(find_segments(seg_root, mosaic, [seg_root]))

    return mosaic

