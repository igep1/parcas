from cv2 import imread
from numpy import load, ndarray, save
import parcas as pca
import os

# __all__ = ["new_from_path", "new_from_array"]
__all__ = ["Slice", "ZSlice"]


class Slice:
    def __init__(self, path=None, rpos=(0, 0), pos=(0, 0), mpos=(0, 0), static_shape=None, con=None, roi=None,
                 overlaps=None, edges=None, parts=None):
        if path:
            assert isinstance(path, dict)
            self.path = path
        else:
            self.path = dict()
        self.rpos = rpos
        self.pos = pos
        self.mpos = mpos
        self.con = con
        self.roi = roi
        # overlaps: right, down right, down, down left, left, up left, up, up right
        if overlaps:
            self.overlaps = overlaps
        else:
            self.overlaps = [(), (), (), (), (), (), (), ()]
        # edges: left, right, up, down
        if edges:
            self.edges = edges
        else:
            self.edges = [[], [], [], []]
        if parts:
            self.parts = parts
        else:
            self.parts = dict()
        if not static_shape:
            self.update_static_shape()
        else:
            self.static_shape = static_shape

    def __repr__(self):
        return "<Slice mpos:%s rpos:%s pos:%s static_shape:%s overlaps:%s edges:%s con:%s path:%s>" %\
               (self.mpos, self.rpos, self.pos, self.static_shape, self.overlaps, self.edges, self.con, self.path)

    def read_image(self, key, z=0, flags=-1):
        """ignores z"""
        if self.path:
            return pca.read_img(self.path[key], flags=flags)

    def update_static_shape(self, key=None):
        if not key:
            key = list(self.path.keys())[0]
        img = self.read_image(key, flags=0)
        if type(img) == ndarray:
            self.static_shape = img.shape[:2]

    def get_overlap_area(self, to_index=4):
        return pca.get_rectangle_cluster_area(self.overlaps[:to_index])

    def ffc(self, ff, df, out_path, rgb_key="rgb", gray=True):
        img = self.read_image(key = rgb_key)
        img = pca.ffc(img, ff, df, gray=gray)
        self.path["ffc"] = out_path + self.path[rgb_key].split("/")[-1].split(".")[-2] + ".npy"
        save(self.path["ffc"], img)


class ZSlice(Slice):
    def __init__(self, *args, **kwargs):
        self.monomer_plane = 0
        super(ZSlice, self).__init__(*args, **kwargs)

    def read_image(self, key, z=None, flags=-1):
        if not z:
            z = self.monomer_plane
        if self.path:
            return pca.read_img(self.path[key][z], flags=flags)

    def update_static_shape(self, key=None):
        if not key:
            key = list(self.path.keys())[0]
        img = self.read_image(key, flags=0)
        l = len(self.path[key])
        if type(img) == ndarray:
            self.static_shape = tuple(list(img.shape[:2]) + [l])
            print(self.static_shape)

    def ffc(self, ff, df, out_path, rgb_key="rgb", gray=True):
        path_list = []
        for i, path in enumerate(self.path[rgb_key]):
            img = self.read_image(key=rgb_key, z=i)
            img = pca.ffc(img, ff, df, gray=gray)
            path_list.append(out_path + self.path[rgb_key][i].split("/")[-1].split(".")[-2] + ".npy")
            save(out_path + self.path[rgb_key][i].split("/")[-1].split(".")[-2] + ".npy", img)
        self.path["ffc"] = path_list
