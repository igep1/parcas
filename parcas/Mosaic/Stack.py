import numpy as np
import cv2
from .Slice import Slice, ZSlice
import os

__all__ = ["Stack"]


class Stack(list):
    def __init__(self, rgb_path, ffc_path, dim, end_on=-1, ff=None, df=None, *args, **kwargs):
        """
        List of ZSlices
        :param rgb_path: path to rgb images
        :param ffc_path: path to ffc images, ff and df required if folder is empty
        :param dim: mosaic dimensions
        :param end_on: image index to end on
        :param ff: flatfield
        :param df: darkfield
        :param args: args of list
        :param kwargs: kwargs of list
        """
        super(Stack, self).__init__(*args, **kwargs)

        files = os.listdir(rgb_path)
        if end_on < 0 or end_on > dim[0] * dim[1]:
            end_on = dim[0] * dim[1]
        shape = np.shape(cv2.imread(rgb_path + files[0], flags=0))[:2]
        if not os.path.exists(ffc_path):
            os.makedirs(ffc_path)
        elif type(ff) == np.ndarray and type(df) == np.ndarray:
            for file in os.listdir(ffc_path):
                os.remove(ffc_path + file)
        i = 0
        while i < end_on:
            if len(dim) == 2:
                path = rgb_path + files[i]
                slc = Slice(path={"rgb": path}, static_shape=shape)
            else:
                paths = [rgb_path + files[i+k] for k in range(dim[2])]
                slc = ZSlice(path={"rgb": paths}, static_shape=tuple(list(shape) + [len(paths)]))
            if type(ff) == np.ndarray and type(df) == np.ndarray:
                slc.ffc(ff, df, ffc_path)
            else:
                if len(dim) == 2:
                    slc.path["ffc"] = ffc_path + ".".join(files[i].split(".")[:-1]) + ".npy"
                else:
                    slc.path["ffc"] = [ffc_path + ".".join(files[i+k].split(".")[:-1]) + ".npy" for k in range(dim[2])]
            self.append(slc)
            if len(dim) == 2:
                i += 1
            else:
                i += dim[2]







