__all__ = ["NotYetImplementedException"]


class NotYetImplementedException(Exception):
    pass

