import parcas as pca
import os
import cv2
import config as cfg
import numpy as np


def main(mosaic=None, to_xml=True):
    #############################################
    # Initialize
    #############################################
    if not mosaic:
        mosaic = pca.mosaic_from_xml(cfg.IO["MOSAIC_XML"])
    dim = mosaic.dim

    if os.path.exists(cfg.IO["DF_AVRG"]):
        df = cv2.imread(cfg.IO["DF_AVRG"])
    else:
        df = pca.avrg_field_from_folder(cfg.IO["DF"])
    if os.path.exists(cfg.IO["FF_AVRG"]):
        ff = cv2.imread(cfg.IO["FF_AVRG"])
    else:
        ff = pca.avrg_field_from_folder(cfg.IO["FF"])

    if not os.path.exists(cfg.IO["FFC"]):
        os.makedirs(cfg.IO["FFC"])
    if not os.path.exists(cfg.IO["LABEL"]):
        os.makedirs(cfg.IO["LABEL"])
    if not os.path.exists(cfg.IO["FFC_MARK"]):
        os.makedirs(cfg.IO["FFC_MARK"])

    #############################################
    # Find Particles
    #############################################
    for j in range(dim[1]):
        for i in range(dim[0]):
            #############################################
            # Create FFC and FFC_BW (Dilated)
            #############################################
            if "ffc" not in mosaic[j][i].path.keys():
                ffc = mosaic[j][i].get_image(key="rgb")
                ffc = pca.ffc(ffc, ff, df)
                # cv2.imwrite(cfg.IO["FFC"] + str(j) + "_" + str(i) + ".png", ffc)
                np.save(cfg.IO["FFC"] + str(j) + "_" + str(i) + ".npy", ffc)
                mosaic[j][i].path["ffc"] = cfg.IO["FFC"] + str(j) + "_" + str(i) + ".npy"
            else:
                ffc = mosaic[j][i].get_image(key="ffc")
                ffc_path = mosaic[j][i].path["ffc"].split("/")
                ffc_path[-1] = str(j) + "_" + str(i) + ".npy"
                ffc_path = "/".join(ffc_path)
                os.rename(mosaic[j][i].path["ffc"], ffc_path)
                mosaic[j][i].path["ffc"] = ffc_path

            ffc_bw = np.around(ffc).astype(np.uint8)
            ffc_bw = cv2.cvtColor(ffc_bw, cv2.COLOR_BGR2GRAY)
            _, ffc_bw = cv2.threshold(ffc_bw, cfg.PROCESS["THRESH_FFC"], 255, cv2.THRESH_BINARY_INV)
            if cfg.PROCESS["DILATE_FFC"]:
                ffc_bw = cv2.dilate(ffc_bw, cfg.PROCESS["DILATION_KERNEL"], iterations=cfg.PROCESS["DILATE_FFC"])
            cv2.imwrite(cfg.IO["FFC_BW_DIL"] + str(j) + "_" + str(i) + ".png", ffc_bw)
            mosaic[j][i].path["ffc_bw_dil"] = cfg.IO["FFC_BW_DIL"] + str(j) + "_" + str(i) + ".png"

            #############################################
            # Find Particles in FFC_BW
            #############################################
            _, labels, stats, _ = cv2.connectedComponentsWithStats(ffc_bw, 8, cv2.CV_32S)
            del ffc_bw
            np.save(cfg.IO["LABEL"] + str(j) + "_" + str(i) + ".npy", labels)
            mosaic[j][i].path["label"] = cfg.IO["LABEL"] + str(j) + "_" + str(i) + ".npy"
            stats = stats[1:]
            # stats = stats[np.lexsort(stats.T[[4]])]
            # stats = stats[:-1]
            mosaic[j][i].parts["full"] = []
            mosaic[j][i].parts["segment"] = []
            mosaic[j][i].parts["overlap"] = []
            mosaic[j][i].parts["unnecessary"] = []
            mosaic[j][i].parts["unknown"] = []
            mosaic[j][i].parts["small"] = []
            slc_shape = mosaic[j][i].static_shape
            print(mosaic[j][i])

            #############################################
            # Sort Particles into Groups
            #############################################
            for label, s in enumerate(stats):
                part = pca.Particle(label=label+1, rpos=(s[0], s[1]), pos=mosaic[j][i].pos + (s[0], s[1]),
                                    mpos=(j, i), static_shape=(s[3], s[2]))
                part.update_touches(mosaic[j][i], to_index=8)
                if s[4] >= cfg.MOSAIC["MIN_AREA"]:
                    if part.touches["mosaic_edge"] < 0:
                        if part.touches["overlap"]:
                            if min(part.touches["overlap"]) >= 4:
                                if part.touches["image_edge"] < 0:
                                    mosaic[j][i].parts["full"].append(part)
                                else:
                                    if part.touches["overlap_edge"]:
                                        mosaic[j][i].parts["segment"].append(part)
                                    else:
                                        mosaic[j][i].parts["unnecessary"].append(part)
                            else:
                                if part.touches["overlap_edge"]:
                                    if part.touches["image_edge"] < 0:
                                        full = True
                                        for t in part.touches["overlap"]:
                                            if t not in part.touches["overlap_edge"]:
                                                full = False
                                                break
                                        if full:
                                            mosaic[j][i].parts["full"].append(part)
                                        else:
                                            mosaic[j][i].parts["overlap"].append(part)
                                    else:
                                        mosaic[j][i].parts["segment"].append(part)
                                else:
                                    mosaic[j][i].parts["overlap"].append(part)
                        else:
                            # analyze: completely inside image, not in any overlap
                            mosaic[j][i].parts["full"].append(part)
                    else:
                        # disregard: on mosaic edge
                        mosaic[j][i].parts["unknown"].append(part)
                else:
                    # disregard: too small
                    mosaic[j][i].parts["small"].append(part)

            #############################################
            # Draw Marked BW Images with Particles
            #############################################
            if cfg.IO["FFC_MARK"]:
                colors = dict(
                    dark_red=(0, 0, 128),
                    red=(0, 0, 255),
                    dark_green=(0, 128, 0),
                    green=(0, 255, 0),
                    dark_blue=(128, 0, 0),
                    blue=(255, 0, 0),
                    light_blue=(255, 255, 0),
                    yellow=(0, 255, 255),
                    pink=(255, 0, 255),
                    purple=(255, 0, 128)
                )
                h, w = mosaic[j][i].static_shape
                h -= 1
                w -= 1
                for e, edge in enumerate(mosaic[j][i].edges):
                    if edge:
                        if e < 2:
                            ffc = cv2.line(ffc, (e*w, edge[0]), (e*w, edge[1]), colors["red"], thickness=1)
                        else:
                            ffc = cv2.line(ffc, (edge[0], (e-2)*h), (edge[1], (e-2)*h), colors["red"], thickness=1)
                for overlap in mosaic[j][i].overlaps[4:]:
                    if overlap:
                        ffc = cv2.rectangle(ffc, overlap[0], overlap[1], colors["blue"], thickness=1)
                for overlap in mosaic[j][i].overlaps[:4]:
                    if overlap:
                        ffc = cv2.rectangle(ffc, overlap[0], overlap[1], colors["green"], thickness=1)

                # particles to disregard
                for part in mosaic[j][i].parts["small"]:
                    ffc = pca.draw_labelled_box(ffc, part, colors["yellow"])
                for part in mosaic[j][i].parts["unknown"]:
                    ffc = pca.draw_labelled_box(ffc, part, colors["red"])
                for part in mosaic[j][i].parts["unnecessary"]:
                    ffc = pca.draw_labelled_box(ffc, part, colors["blue"])
                for part in mosaic[j][i].parts["overlap"]:
                    ffc = pca.draw_labelled_box(ffc, part, colors["green"])
                # particles to analyze
                for part in mosaic[j][i].parts["full"]:
                    ffc = pca.draw_labelled_box(ffc, part, colors["pink"])
                for part in mosaic[j][i].parts["segment"]:
                    ffc = pca.draw_labelled_box(ffc, part, colors["light_blue"])

                cv2.imwrite(cfg.IO["FFC_MARK"] + str(j) + "_" + str(i) + ".png", ffc)
            del ffc

    #############################################
    # Stitch Segmented Particles (untested, extremely rare case)
    #############################################
    stitch = False
    if stitch:
        def find_segments(seg_root, mosaic, group):
            for o in seg_root.touches["overlap"]:
                idx = seg_root.mpos + pca.NEIGHBOR_INDICES[o]
                for seg_branch in mosaic[idx[0]][idx[1]].parts["segment"]:
                    if o+4 in seg_branch.touches["overlap"]:
                        if not seg_branch.belongs_to_group:
                            if pca.get_overlap(seg_root, seg_branch):
                                seg_branch.belongs_to_group = True
                                group.append(seg_branch)
                                group = find_segments(seg_branch, mosaic, group)
            return group

        groups = []
        for j in range(dim[1]):
            for i in range(dim[0]):
                for seg_root in mosaic[j][i].parts["segment"]:
                    if not seg_root.belongs_to_group:
                        seg_root.belongs_to_group = True
                        groups.append(find_segments(seg_root, mosaic, [seg_root]))

    if to_xml:
        print("Writing " + cfg.IO["MOSAIC_XML"])
        pca.mosaic_to_xml(mosaic, cfg.IO["MOSAIC_XML"])
    return mosaic


if __name__ == "__main__":
    if cfg.BOOL["COPY_CONFIG"]:
        from shutil import copyfile
        copyfile("config.py", cfg.IO["PROJECT"] + "config.py")

    main()

    if cfg.BOOL["PLAY_DONE_SOUND"] and os.path.exists("done_sound.mp3"):
        from playsound import playsound
        playsound("done_sound.mp3")

