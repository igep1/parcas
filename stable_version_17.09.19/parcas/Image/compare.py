import numpy as np
import cv2
from .. import constants as const
from .. import display

__all__ = ["get_rois", "rpos_equal_at", "get_rpos", "get_overlap", "calc_edge", "get_rectangle_cluster_area"]


def get_rois(arr, s, direction, min_area, min_edge_dist, n, max_part=20, f=1):
    ishp = arr.shape
    ih = ishp[0]
    iw = ishp[1]
    x1 = []
    y1 = []
    w1 = []
    h1 = []
    x2 = []
    y2 = []
    w2 = []
    h2 = []
    v = 0

    if direction == const.RIGHT:
        s = s[np.lexsort(s.T[[0]])]
        s = s[::-1]
    elif direction == const.LEFT:
        s = s[np.lexsort(s.T[[0]])]
    elif direction == const.DOWN:
        s = s[np.lexsort(s.T[[1]])]
        s = s[::-1]
        v = 1
    elif direction == const.UP:
        s = s[np.lexsort(s.T[[1]])]
        v = 1
    else:
        print("Invalid direction.")

    found_k = 0
    for k in range(len(s)):
        if min_area * max_part > s[k, 4] >= min_area and s[k, 1-v] >= min_edge_dist\
                and s[k, 1-v] + s[k, 3-v] <= ishp[v] - min_edge_dist:
            if len(x1) <= len(x2):
                x1.append(s[k, 0])
                y1.append(s[k, 1])
                w1.append(s[k, 2])
                h1.append(s[k, 3])
            else:
                x2.append(s[k, 0])
                y2.append(s[k, 1])
                w2.append(s[k, 2])
                h2.append(s[k, 3])
            found_k += 1
            if found_k == 2*n:
                break
    roi1 = [0]*5
    roi2 = [0]*5
    if direction == const.RIGHT:
        roi1[0] = min(x1)
        roi1[1] = min(y1)
        roi1[2] = iw - roi1[0]
        roi1[3] = max(map(lambda y0, h0: y0 + h0, y1, h1)) - roi1[1]

        roi2[0] = min(x2)
        roi2[1] = min(y2)
        roi2[2] = iw - roi2[0]
        roi2[3] = max(map(lambda y0, h0: y0 + h0, y2, h2)) - roi2[1]
    elif direction == const.LEFT:
        roi1[0] = 0
        roi1[1] = min(y1)
        roi1[2] = max(map(lambda x0, w0: x0 + w0, x1, w1))
        roi1[3] = max(map(lambda y0, h0: y0 + h0, y1, h1)) - roi1[1]

        roi2[0] = 0
        roi2[1] = min(y2)
        roi2[2] = max(map(lambda x0, w0: x0 + w0, x2, w2))
        roi2[3] = max(map(lambda y0, h0: y0 + h0, y2, h2)) - roi2[1]
    elif direction == const.DOWN:
        roi1[0] = min(x1)
        roi1[1] = min(y1)
        roi1[2] = max(map(lambda x0, w0: x0 + w0, x1, w1)) - roi1[0]
        roi1[3] = ih - roi1[1]

        roi2[0] = min(x2)
        roi2[1] = min(y2)
        roi2[2] = max(map(lambda x0, w0: x0 + w0, x2, w2)) - roi2[0]
        roi2[3] = ih - roi2[1]
    elif direction == const.UP:
        roi1[0] = min(x1)
        roi1[1] = 0
        roi1[2] = max(map(lambda x0, w0: x0 + w0, x1, w1)) - roi1[0]
        roi1[3] = max(map(lambda y0, h0: y0 + h0, y1, h1))

        roi2[0] = min(x2)
        roi2[1] = 0
        roi2[2] = max(map(lambda x0, w0: x0 + w0, x2, w2)) - roi2[0]
        roi2[3] = max(map(lambda y0, h0: y0 + h0, y2, h2))
    roi1[4] = arr[roi1[1]:roi1[1] + roi1[3], roi1[0]:roi1[0] + roi1[2]]
    roi2[4] = arr[roi2[1]:roi2[1] + roi2[3], roi2[0]:roi2[0] + roi2[2]]

    # show_rois = False
    # if show_rois and (f == 85):
    #     print(x1)
    #     print(y1)
    #     print(w1)
    #     print(h1)
    #     img = img.copy()
    #
    #     cv2.rectangle(img, (roi1[0], roi1[1]), (roi1[0] + roi1[2], roi1[1] + roi1[3]), 100, thickness=4)
    #     cv2.rectangle(img, (roi2[0], roi2[1]), (roi2[0] + roi2[2], roi2[1] + roi2[3]), 100, thickness=4)
    #     show(img, 3)

    return roi1, roi2


def rpos_equal_at(new_max, max_list, rel_rois, num):
    for i, m in enumerate(max_list):
        max_rel = num * np.subtract(m, new_max)
        if max_rel[0] == rel_rois[0] and max_rel[1] == rel_rois[1]:
            return i
    return None


def get_rpos(arr, roi1, roi2, fail_count):
    rel_rois = (roi1[0] - roi2[0], roi1[1] - roi2[1])
    match1 = cv2.matchTemplate(arr, roi1[4], cv2.TM_CCOEFF)
    match2 = cv2.matchTemplate(arr, roi2[4], cv2.TM_CCOEFF)

    max1_list = []
    max2_list = []
    _, _, _, max1 = cv2.minMaxLoc(match1)
    _, _, _, max2 = cv2.minMaxLoc(match2)
    max1_list.append(max1)
    max2_list.append(max2)

    eq2 = 0
    eq1 = rpos_equal_at(max2, max1_list, rel_rois, 1)

    while eq1 is None or eq2 is None:
        match1[max1_list[-1][1], max1_list[-1][0]] = 0
        _, _, _, max1 = cv2.minMaxLoc(match1)
        max1_list.append(max1)
        eq1 = len(max1_list) - 1
        eq2 = rpos_equal_at(max1, max2_list, rel_rois, -1)
        if eq2 is not None:
            break

        match2[max2_list[-1][1], max2_list[-1][0]] = 0
        _, _, _, max2 = cv2.minMaxLoc(match2)
        max2_list.append(max2)
        eq2 = len(max2_list) - 1
        eq1 = rpos_equal_at(max2, max1_list, rel_rois, 1)

        fail_count -= 1
        if fail_count == 0:
            return None

    return max1_list[eq1][0] - roi1[0], max1_list[eq1][1] - roi1[1]


'''
def get_overlap(a, b, img_w, img_h):
    if a[1] > b[1]:
        tl_y = a[1] - b[1]
        br_y = img_h - 1
        if a[0] > b[0]:
            tl_x = a[0] - b[0]
            br_x = img_w - 1
        else:
            tl_x = 0
            br_x = img_w - 1 - (b[0] - a[0])
    else:
        tl_y = 0
        br_y = img_h - 1 - (b[1] - a[1])
        if a[0] > b[0]:
            tl_x = a[0] - b[0]
            br_x = img_w - 1
        else:
            tl_x = 0
            br_x = img_w - 1 - (b[0] - a[0])
    return (tl_x, tl_y), (br_x, br_y)
'''


def get_overlap(slc1, slc2):
    if type(slc1) == np.ndarray:
        shape = slc1.shape
    else:
        shape = slc1.static_shape
    if slc2.pos[1] > slc1.pos[1]:
        tl_y = slc2.pos[1] - slc1.pos[1]
        br_y = shape[0] - 1
        if slc2.pos[0] > slc1.pos[0]:
            tl_x = slc2.pos[0] - slc1.pos[0]
            br_x = shape[1] - 1
        else:
            tl_x = 0
            br_x = shape[1] - 1 - (slc1.pos[0] - slc2.pos[0])
    else:
        tl_y = 0
        br_y = shape[0] - 1 - (slc1.pos[1] - slc2.pos[1])
        if slc2.pos[0] > slc1.pos[0]:
            tl_x = slc2.pos[0] - slc1.pos[0]
            br_x = shape[1] - 1
        else:
            tl_x = 0
            br_x = shape[1] - 1 - (slc1.pos[0] - slc2.pos[0])
    if tl_x >= 0 and tl_y >= 0 and br_x >= 0 and br_y >= 0:
        return (tl_x, tl_y), (br_x, br_y)
    else:
        return None, None


'''
def calc_edge(dots, l):
    s = 0
    e = l
    if dots:
        for dot in dots:
            if dot[0] <= s:
                s = max(dot[1], s)
                if s == e:
                    return []
            else:
                e = min(dot[0], e)
    return [s, e]
'''


def calc_edge(dots, l):
    s = 0
    e = l
    if dots:
        for dot in dots:
            if dot[0] == 0:
                s = max(dot[1], s)
            elif dot[1] == l:
                e = min(dot[0], e)
    if s > e:
        return ()
    else:
        return s, e


def get_rectangle_cluster_area(rects):
    """Return the total area of any number of (overlapping) rectangles."""
    rects = [rect for rect in rects if rect]
    if len(rects) == 0:
        # print("0")
        return 0
    elif len(rects) == 1:
        # print("1")
        return (rects[0][1][0] - rects[0][0][0]) * (rects[0][1][1] - rects[0][0][1])
    elif len(rects) == 2:
        dx = min(rects[0][1][0], rects[1][1][0]) - max(rects[0][0][0], rects[1][0][0])
        dy = min(rects[0][1][1], rects[1][1][1]) - max(rects[0][0][1], rects[1][0][1])
        a0 = (rects[0][1][0] - rects[0][0][0]) * (rects[0][1][1] - rects[0][0][1])
        a1 = (rects[1][1][0] - rects[1][0][0]) * (rects[1][1][1] - rects[1][0][1])
        if (dx >= 0) and (dy >= 0):
            # print("2a")
            return a0 + a1 - dx*dy
        else:
            # print("2b")
            return a0 + a1
    else:
        xmax = 0
        ymax = 0
        for rect in rects:
            xmax = max(xmax, rect[1][0])
            ymax = max(ymax, rect[1][1])
        img = np.zeros((ymax, xmax), dtype=np.uint8)
        for rect in rects:
            img = cv2.rectangle(img, rect[0], rect[1], 255, thickness=-1)
        # print("3+")
        # display.show(img, size=0.3, name="img", t=1)
        return np.count_nonzero(img)


