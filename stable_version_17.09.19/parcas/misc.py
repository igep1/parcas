__all__ = ["contains_any"]


def contains_any(iter1, iter2):
    for item in iter2:
        if item in iter1:
            return True
    return False



