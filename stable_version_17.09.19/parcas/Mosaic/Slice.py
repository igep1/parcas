from cv2 import imread
from numpy import load
from ..Image import compare

# __all__ = ["new_from_path", "new_from_array"]
__all__ = ["Slice"]


class Slice:
    def __init__(self, path=None, rpos=(0, 0), pos=(0, 0), mpos=(0, 0), static_shape=None, con=None, roi=None,
                 overlaps=None, edges=None, parts=None):
        if path:
            assert isinstance(path, dict)
            self.path = path
        else:
            self.path = dict()
        self.rpos = rpos
        self.pos = pos
        self.mpos = mpos
        self.con = con
        self.roi = roi
        # overlaps: right, down right, down, down left, left, up left, up, up right
        if overlaps:
            self.overlaps = overlaps
        else:
            self.overlaps = [(), (), (), (), (), (), (), ()]
        # edges: left, right, up, down
        if edges:
            self.edges = edges
        else:
            self.edges = [[], [], [], []]
        if parts:
            self.parts = parts
        else:
            self.parts = dict()
        if not static_shape:
            self.update_static_shape()
        else:
            self.static_shape = static_shape

    def __repr__(self):
        return "<Slice mpos:%s rpos:%s pos:%s static_shape:%s path:%s overlaps:%s edges:%s con:%s>" %\
               (self.mpos, self.rpos, self.pos, self.static_shape, self.path, self.overlaps, self.edges, self.con)

    def get_image(self, key, flags=-1):
        if self.path:
            if self.path[key].split(".")[-1] == "npy":
                return load(self.path[key])
            else:
                return imread(self.path[key], flags=flags)

    def update_static_shape(self, key=None):
        if not key:
            key = list(self.path.keys())[0]
        img = self.get_image(key, flags=0)
        if img:
            self.static_shape = img.shape[:2]

    def get_overlap_area(self, to_index=4):
        return compare.get_rectangle_cluster_area(self.overlaps[:to_index])

