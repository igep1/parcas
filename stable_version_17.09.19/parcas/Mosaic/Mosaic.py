from .. import constants as const
from ..Image import compare

# __all__ = ["new"]
__all__ = ["Mosaic"]


class Mosaic(list):
    def __init__(self, dim, stack=None, start_pos=const.TOP_RIGHT, static_shape=(0, 0), origin=(0, 0), *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.dim = dim
        self.start_pos = start_pos
        self.stack = stack
        self.static_shape = static_shape
        self.origin = origin
        if self.stack:
            self.update()

    def __repr__(self):
        rep = "<Mosaic dim:%s start_pos:%s static_shape:%s origin:%s" %\
              (self.dim, self.start_pos, self.static_shape, self.origin)
        rep += "\n["
        for j in range(self.dim[1]):
            for i in range(self.dim[0]):
                if not (j == self.dim[1] - 1 and i == self.dim[0] - 1):
                    rep += str(self[j][i]) + ",\n"
        rep += str(self[self.dim[1]-1][self.dim[0]-1]) + "]>"
        return rep

    def update(self):
        self.static_shape = self.calc_shape()
        self.origin = self.calc_origin()
        self.calc_pos()
        self.clear()
        if self.start_pos == const.TOP_RIGHT:
            d = -1
        elif self.start_pos == const.TOP_LEFT:
            d = 1
        else:
            print("NYI: BOTTOM_LEFT, BOTTOM_RIGHT")
        for j in range(self.dim[1]):
            arr = [None for x in range(self.dim[0])]
            for i in range(self.dim[0]):
                if d < 0:
                    m = (j + 1) * self.dim[0] - i - 1
                else:
                    m = j * self.dim[0] + i
                self.stack[m].mpos = (j, i)
                arr[i] = self.stack[m]
            self.append(arr)
            d *= -1
        self.calc_overlaps()

    def calc_shape(self):
        s = 0
        if self.start_pos == const.TOP_RIGHT:
            s = -1
        elif self.start_pos == const.TOP_LEFT:
            s = 1
        else:
            print("NYI: BOTTOM_LEFT, BOTTOM_RIGHT")
        x_dim = y_dim = x_sum = y_sum = x_add = y_add = 0
        for slc in self.stack:
            x_sum += slc.rpos[0]
            y_sum += slc.rpos[1]
            if abs(x_sum) > x_dim:
                x_dim = abs(x_sum)
            if s * x_sum < 0:
                x_add = abs(x_sum)
            if abs(y_sum) > y_dim:
                y_dim = abs(y_sum)
            if y_sum < 0:
                y_add = abs(y_sum)
        slc = self.stack[0]
        return y_dim + y_add + slc.static_shape[0], x_dim + x_add + slc.static_shape[1]

    def calc_origin(self):
        s_x = 0
        x_list = [0]
        x_sum = 0
        for slc in self.stack:
            x_sum += slc.rpos[0]
            x_list.append(x_sum)
        slc = self.stack[0]
        if self.start_pos == 1:
            s_x = self.static_shape[1] - slc.static_shape[1] - max(x_list)
        elif self.start_pos == 0:
            s_x = 0 - min(x_list)
        else:
            print("NYI: BOTTOM_LEFT, BOTTOM_RIGHT")
        y_list = [0]
        y_sum = 0
        for q in range(self.dim[0] - 1):
            y_sum += self.stack[q].rpos[1]
            y_list.append(y_sum)
        s_y = 0 - min(y_list)
        return s_x, s_y

    def calc_pos(self):
        x, y = self.origin[0], self.origin[1]
        for slc in self.stack:
            x += slc.rpos[0]
            y += slc.rpos[1]
            slc.pos = (x, y)

    def calc_overlaps(self):
        for j in range(self.dim[1]):
            for i in range(self.dim[0]):
                left_dots = []
                right_dots = []
                up_dots = []
                down_dots = []
                overlaps = [(), (), (), (), (), (), (), ()]

                # Draw overlap for right, down right, down and down left neighbors
                if i + 1 < self.dim[0]:  # right neighbor?
                    tl, br = compare.get_overlap(self[j][i], self[j][i + 1])
                    if tl and br:
                        right_dots.append((tl[1], br[1]))
                        if tl[1] == 0:
                            up_dots.append((tl[0], br[0]))
                        else:
                            down_dots.append((tl[0], br[0]))
                        overlaps[0] = (tl, br)
                        if j + 1 < self.dim[1]:  # down right neighbor?
                            tl, br = compare.get_overlap(self[j][i], self[j + 1][i + 1])
                            if tl and br:
                                right_dots.append((tl[1], br[1]))
                                down_dots.append((tl[0], br[0]))
                                overlaps[1] = (tl, br)
                if j + 1 < self.dim[1]:  # down neighbor?
                    tl, br = compare.get_overlap(self[j][i], self[j + 1][i])
                    if tl and br:
                        down_dots.append((tl[0], br[0]))
                        if tl[0] == 0:
                            left_dots.append((tl[1], br[1]))
                        else:
                            right_dots.append((tl[1], br[1]))
                        overlaps[2] = (tl, br)
                        if i - 1 >= 0:  # down left neighbor?
                            tl, br = compare.get_overlap(self[j][i], self[j + 1][i - 1])
                            if tl and br:
                                down_dots.append((tl[0], br[0]))
                                left_dots.append((tl[1], br[1]))
                                overlaps[3] = (tl, br)

                #############################################
                # Find Mosaic Edge (dots are also collected above)
                #############################################
                # Only calculate overlap for left, up left, up and up right neighbors to get the edge of the image
                if i - 1 >= 0:  # left neighbor?
                    tl, br = compare.get_overlap(self[j][i], self[j][i - 1])
                    if tl and br:
                        left_dots.append((tl[1], br[1]))
                        if tl[1] == 0:
                            up_dots.append((tl[0], br[0]))
                        else:
                            down_dots.append((tl[0], br[0]))
                        overlaps[4] = (tl, br)
                        if j - 1 >= 0:  # up left neighbor?
                            tl, br = compare.get_overlap(self[j][i], self[j - 1][i - 1])
                            if tl and br:
                                up_dots.append((tl[0], br[0]))
                                left_dots.append((tl[1], br[1]))
                                overlaps[5] = (tl, br)
                if j - 1 >= 0:  # up neighbor?
                    tl, br = compare.get_overlap(self[j][i], self[j - 1][i])
                    if tl and br:
                        up_dots.append((tl[0], br[0]))
                        if tl[0] == 0:
                            left_dots.append((tl[1], br[1]))
                        else:
                            right_dots.append((tl[1], br[1]))
                        overlaps[6] = (tl, br)
                        if i + 1 < self.dim[0]:  # up right neighbor?
                            tl, br = compare.get_overlap(self[j][i], self[j - 1][i + 1])
                            if tl and br:
                                up_dots.append((tl[0], br[0]))
                                right_dots.append((tl[1], br[1]))
                                overlaps[7] = (tl, br)
                self[j][i].overlaps = overlaps.copy()

                left_edge = compare.calc_edge(left_dots, self[j][i].static_shape[0] - 1)
                right_edge = compare.calc_edge(right_dots, self[j][i].static_shape[0] - 1)
                up_edge = compare.calc_edge(up_dots, self[j][i].static_shape[1] - 1)
                down_edge = compare.calc_edge(down_dots, self[j][i].static_shape[1] - 1)
                self[j][i].edges = [left_edge, right_edge, up_edge, down_edge]

# def new(dim, stack=None, start_pos=1, *args, **kwargs):
#     return Mosaic(dim, stack=stack, start_pos=start_pos, *args, **kwargs)




