import pandas as pd
import xml.etree.ElementTree as ET
import cv2
import ast
from .Mosaic import Mosaic
from .Mosaic import Slice
from .Particle import Particle
from PIL import Image, ImageDraw

__all__ = ["mosaic_to_xml", "mosaic_from_xml", "mosaic_to_png", "parse_con_data", "write_mosaic_data", "load_bin_image"]


def mosaic_to_xml(mosaic, path):
    root_elem = ET.Element("mosaic",
                           dim=mosaic.dim,
                           start_pos=str(mosaic.start_pos),
                           static_shape=mosaic.static_shape,
                           origin=mosaic.origin)
    if mosaic:
        for j in range(mosaic.dim[1]):
            for i in range(mosaic.dim[0]):
                slc = mosaic[j][i]
                slc_elem = ET.SubElement(root_elem, "slice",
                                         mpos=(j, i),
                                         rpos=slc.rpos,
                                         pos=slc.pos,
                                         # roi=slc.roi,
                                         overlaps=slc.overlaps,
                                         edges=slc.edges,
                                         static_shape=slc.static_shape,
                                         con=slc.con)
                ET.SubElement(slc_elem, "path", slc.path)
                parts_elem = ET.SubElement(slc_elem, "parts")
                for key in slc.parts.keys():
                    key_elem = ET.SubElement(parts_elem, key)
                    for part in slc.parts[key]:
                        if part.belongs_to_group:
                            belongs_to_group = 1
                        else:
                            belongs_to_group = 0
                        part_elem = ET.SubElement(key_elem, "particle",
                                                  mpos=(j, i),
                                                  label=str(part.label),
                                                  rpos=part.rpos,
                                                  pos=part.pos,
                                                  static_shape=part.static_shape,
                                                  belongs_to_group=str(belongs_to_group),
                                                  luminance=str(part.luminance),
                                                  area=str(part.area),
                                                  mass_lum=str(part.mass_lum),
                                                  mass_area=str(part.mass_area),
                                                  gyrad=str(part.gyrad),
                                                  d2=str(part.d2),
                                                  d3=str(part.d3))
                        ET.SubElement(part_elem, "path", part.path)
                        touches = dict()
                        for t_key in part.touches:
                            touches[t_key] = str(part.touches[t_key])
                        ET.SubElement(part_elem, "touches", touches)
    tree = ET.ElementTree(root_elem)
    tree.write(path)


def mosaic_from_xml(path):
    tree = ET.parse(path)
    root_elem = tree.getroot()
    dim = str2tuple(root_elem.attrib["dim"])
    origin = str2tuple(root_elem.attrib["origin"])
    start_pos = int(float(root_elem.attrib["start_pos"]))
    static_shape = str2tuple(root_elem.attrib["static_shape"])
    mosaic = Mosaic(dim, start_pos=start_pos, origin=origin, static_shape=static_shape)
    for j in range(dim[1]):
        mosaic.append([None for i in range(dim[0])])
    for slc_elem in root_elem:
        mpos = str2tuple(slc_elem.attrib["mpos"])
        rpos_slc = str2tuple(slc_elem.attrib["rpos"])
        pos_slc = str2tuple(slc_elem.attrib["pos"])
        static_shape_slc = str2tuple(slc_elem.attrib["static_shape"])
        overlaps = ast.literal_eval(slc_elem.attrib["overlaps"])
        edges = ast.literal_eval(slc_elem.attrib["edges"])
        con = ast.literal_eval(slc_elem.attrib["con"])
        path_slc = slc_elem.find("path").attrib
        parts_elem = slc_elem.find("parts")
        new_parts = dict()
        for key_elem in parts_elem:
            new_parts[key_elem.tag] = []
            for part_elem in key_elem:
                label = int(float(part_elem.attrib["label"]))
                rpos = str2tuple(part_elem.attrib["rpos"])
                pos = str2tuple(part_elem.attrib["pos"])
                static_shape = str2tuple(part_elem.attrib["static_shape"])
                luminance = int(float(part_elem.attrib["luminance"]))
                area = int(float(part_elem.attrib["area"]))
                gyrad = int(float(part_elem.attrib["gyrad"]))
                mass_lum = str2tuple(part_elem.attrib["mass_lum"])
                mass_area = str2tuple(part_elem.attrib["mass_area"])
                d2 = str2tuple(part_elem.attrib["d2"])
                d3 = str2tuple(part_elem.attrib["d3"])
                if part_elem.attrib["belongs_to_group"] == "0":
                    belongs_to_group = False
                else:
                    belongs_to_group = True
                path = part_elem.find("path").attrib
                touches = dict()
                for t_key, t_val in part_elem.find("touches").attrib.items():
                    try:
                        t = int(float(t_val))
                    except ValueError:
                        t = ast.literal_eval(t_val)
                    touches[t_key] = t
                part = Particle(mpos=mpos, label=label, rpos=rpos, pos=pos, static_shape=static_shape,
                                luminance=luminance, area=area, mass_lum=mass_lum, mass_area=mass_area, gyrad=gyrad,
                                d2=d2, d3=d3, path=path, touches=touches, belongs_to_group=belongs_to_group)
                new_parts[key_elem.tag].append(part)

        mosaic[mpos[0]][mpos[1]] = Slice(mpos=mpos, path=path_slc, rpos=rpos_slc, pos=pos_slc,
                                         static_shape=static_shape_slc, overlaps=overlaps, edges=edges, con=con,
                                         parts=new_parts)
    return mosaic


def mosaic_to_png(mosaic, path, key, mode="RGB", bg_color=0, pad=(0., 0.), draw_outlines=False, show_progress=False):
    cvs_h, cvs_w = mosaic.static_shape
    csv_pad = (int(round(pad[0]*cvs_w)), int(round(pad[1]*cvs_h)))
    if bg_color != 0:
        cvs = Image.new(mode, (cvs_w + 2 * csv_pad[0], cvs_h + 2 * csv_pad[1]), bg_color)
    else:
        cvs = Image.new(mode, (cvs_w + 2 * csv_pad[0], cvs_h + 2 * csv_pad[1]))
    for j in range(mosaic.dim[1]):
        for i in range(mosaic.dim[0]):
            slc = mosaic[j][i]
            if show_progress:
                print(slc)
            img = Image.open(slc.path[key], "r")
            cvs.paste(img, (slc.pos[0] + csv_pad[0], slc.pos[1] + csv_pad[1]))
    if draw_outlines:
        draw = ImageDraw.Draw(cvs)
        for j in range(mosaic.dim[1]):
            for i in range(mosaic.dim[0]):
                slc = mosaic[j][i]
                tl = (slc.pos[0] + csv_pad[0], slc.pos[1] + csv_pad[1])
                br = (tl[0] + slc.static_shape[1], tl[1] + slc.static_shape[0])
                draw.rectangle((tl, br), outline=(255,)*len(cvs.getbands()), width=1)
    if show_progress:
        print("Saving Mosaic to " + str(path) + " (" +
              "mode: " + str(mode) + ", " +
              "key: " + str(key) + ", " +
              "pad: " + str(pad) + ")")
    cvs.save(path, format="png")


def str2tuple(tuple_string, asint=True):
    if tuple_string == "()":
        return ()
    else:
        s = tuple_string[1:-1].replace(" ", "").split(",")
        for i in range(len(s)):
            s[i] = float(s[i])
            if asint:
                s[i] = int(s[i])
        return tuple(s)


def parse_con_data(path):
    def parse_step(con_img):
        cons = []
        for c in con_img:
            if len(list(c)) != 0:
                con_origin = c[0].text.strip()[1:-1].split(",")
                con_origin = (int(float(con_origin[0])), int(float(con_origin[1])))
                con_type = int(float(c.attrib["type"]))
                if con_type in (0, 1):
                    con_region = c[1].text.strip()[1:-1].split(",")
                    con_region = (int(float(con_region[0])), int(float(con_region[1])))
                else:
                    con_region = c[1].text.strip()[2:-2].split("],[")
                    new_region = []
                    for reg in con_region:
                        reg_str = reg.split(",")
                        reg_int = []
                        for rs in reg_str:
                            reg_int.append(int(float(rs)))
                        del reg_str
                        new_region.append(reg_int)
                    con_region = new_region
                    del new_region
                cons.append([con_origin, con_region, con_type])
        return cons

    con_list = ET.parse(path).getiterator("image")
    new_con_list = []
    for elem in con_list:
        new_con_list.append(parse_step(elem))
    con_list = new_con_list
    del new_con_list

    return con_list


def write_mosaic_data(path, real_mat, mosaic_shape, roi_mat, sample_area):
    df_mosaic_shape = pd.DataFrame({"mosaic_shape": mosaic_shape})
    df_sample_area = pd.DataFrame({"sample_area": [sample_area]})
    df_dim = pd.DataFrame({"dim": (len(real_mat[0]), len(real_mat))})
    df_real = pd.DataFrame(columns=["real_x", "real_y"])
    df_roi = pd.DataFrame(columns=["roi1_x", "roi1_y", "roi1_w", "roi1_h", "roi2_x", "roi2_y", "roi2_w", "roi2_h"])
    for j in range(len(real_mat)):
        for i in range(len(real_mat[j])):
            df_real = df_real.append({"real_x": real_mat[j][i][0], "real_y": real_mat[j][i][1]}, ignore_index=True)
            if roi_mat[j][i][0]:
                df_roi = df_roi.append({"roi1_x": roi_mat[j][i][0][0], "roi1_y": roi_mat[j][i][0][1],
                                        "roi1_w": roi_mat[j][i][0][2], "roi1_h": roi_mat[j][i][0][3],
                                        "roi2_x": roi_mat[j][i][1][0], "roi2_y": roi_mat[j][i][1][1],
                                        "roi2_w": roi_mat[j][i][1][2], "roi2_h": roi_mat[j][i][1][3]}, ignore_index=True)
    df = pd.concat([df_mosaic_shape, df_sample_area, df_dim, df_real, df_roi], ignore_index=True, axis=1)
    df.columns = ["mosaic_shape", "sample_area", "dim", "real_x", "real_y", "roi1_x", "roi1_y", "roi1_w", "roi1_h",
                  "roi2_x", "roi2_y", "roi2_w", "roi2_h"]
    df.to_csv(path + "mosaic_data.csv")


def load_bin_image(file_name, bin_out):
    # Read Image
    img = cv2.imread(bin_out + file_name)
    # Apply Threshold
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, img = cv2.threshold(img, 100, 255, cv2.THRESH_BINARY)
    return img




