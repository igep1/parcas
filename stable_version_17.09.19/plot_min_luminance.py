import config as cfg
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import os

# BIN_SIZE = 20
# MIN_LUM = 9000
# MAX_LUM = 30000
BIN_SIZE = 1.
MIN_LUM = 0
MAX_LUM = 300
ROUND_TO = 0
SCL = 0.75
PLT_GAUSS = False
SAVE_FIG = False

if not os.path.exists(cfg.IO["PLOTS"]) and SAVE_FIG:
    os.makedirs(cfg.IO["PLOTS"])

plt.figure(figsize=(11, 7))
font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22*SCL}
plt.rc('font', **font)

data = pd.read_csv(cfg.IO["DATA"])
# data = pd.read_csv("E:/parcas/S0/B0/data.csv")
luminance = np.array(data["luminance"], dtype=np.uint32)
luminance = luminance[luminance <= MAX_LUM]
luminance = luminance[luminance >= MIN_LUM]
print("Particles\t" + str(len(luminance)))
print("Min Lum\t" + str(min(luminance)))
print("Max Lum\t" + str(max(luminance)))
mean = np.mean(luminance)
print("Mean\t\t" + str(mean))
variance = np.var(luminance)
print("Variance\t" + str(variance))
stdev = np.sqrt(variance)
print("Stand. Dev.\t" + str(stdev))
res = plt.hist(luminance, color="dimgrey", bins=int(round((MAX_LUM-MIN_LUM)/BIN_SIZE)))
# mark1 = 10200
mark1 = 90
plt.axvline(x=mark1, color="red", alpha=0.5, linestyle="--", linewidth=3*SCL)
plt.text(x=mark1+BIN_SIZE, y=2000, s=str(mark1), fontsize=22*SCL, color="red", alpha=0.5)
# mark2 = 16300
mark2 = 150
plt.axvline(x=mark2, color="red", alpha=0.5, linestyle="--", linewidth=3*SCL)
plt.text(x=mark2+BIN_SIZE, y=2000, s=str(mark2), fontsize=22*SCL, color="red", alpha=0.5)
plt.xlim(MIN_LUM, MAX_LUM)

if PLT_GAUSS:
    x = np.linspace(min(luminance), max(luminance), 100)
    dx = res[1][1] - res[1][0]
    scale = len(luminance) * dx
    plt.plot(x, scipy.stats.norm.pdf(x, mean, stdev) * scale, color="red", linewidth=3*SCL)
    plt.axvline(x=mean-stdev, color="red", alpha=0.5, linestyle="--", linewidth=3*SCL)
    plt.axvline(x=mean+stdev, color="red", alpha=0.5, linestyle="--", linewidth=3*SCL)
    plt.text(MIN_LUM, 0.95*max(res[0]),
             "\u03BC=" + str(np.around(mean, decimals=ROUND_TO).astype(np.uint16)) +
             ", \u03C3=" + str(np.around(stdev, decimals=ROUND_TO).astype(np.uint16)),
             fontsize=22*SCL, bbox={'facecolor': 'white', 'alpha': 1, 'pad': 4*SCL, 'linewidth': 2*SCL})

plt.xlabel("Luminance")
plt.ylabel("Quantity")
ax = plt.gca()

ax.tick_params(width=2*SCL, length=7*SCL)
for axis in ['top', 'bottom', 'left', 'right']:
    ax.spines[axis].set_linewidth(2*SCL)

plt.tight_layout()
if SAVE_FIG:
    plt.savefig(cfg.IO["PLOTS"] + "min_lum.png", dpi=300)
plt.show()





