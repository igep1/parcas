import config as cfg
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import os

MIN_AREA = 0
MAX_AREA = 600
MIN_LUM = 0
MAX_LUM = 80000
MIN_MASS = 0
MAX_MASS = 30
ROUND_TO = 0
SCL = 0.75
MAX_MASS_QUANT = 20000

if not os.path.exists(cfg.IO["PLOTS"]):
    os.makedirs(cfg.IO["PLOTS"])

data = pd.read_csv(cfg.IO["DATA"])
area = np.array(data["area"], dtype=np.uint32)
lum = np.array(data["luminance"], dtype=np.uint64)
mass_area = np.array(data["mass_area"], dtype=np.float64)
mass_area = np.around(mass_area)
mass_lum = np.array(data["mass_lum"], dtype=np.float64)
mass_lum = np.around(mass_lum)

#############################################
# Plot Lum Histogram
#############################################
plt.figure(figsize=(11, 7))
font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22*SCL}
plt.rc('font', **font)

if not MAX_LUM:
    MAX_LUM = np.amax(lum)
lum = lum[lum <= MAX_LUM]
lum = lum[lum >= MIN_LUM]

plt.hist(lum, color="dimgrey", bins=int(round((min(MAX_LUM, np.amax(lum))-MIN_LUM)/100)))
mono = 10300
plt.axvline(x=mono, alpha=0.5, color="red", linestyle="--", linewidth=3*SCL)
plt.axvline(x=2*mono, alpha=0.5, color="red", linestyle="--", linewidth=3*SCL)
plt.axvline(x=3*mono, alpha=0.5, color="red", linestyle="--", linewidth=3*SCL)
plt.axvline(x=4*mono, alpha=0.5, color="red", linestyle="--", linewidth=3*SCL)

plt.xlabel("Brightness")
plt.ylabel("Quantity")
plt.xlim(left=MIN_LUM, right=MAX_LUM)
ax = plt.gca()

ax.tick_params(width=2*SCL, length=7*SCL)
for axis in ['top', 'bottom', 'left', 'right']:
    ax.spines[axis].set_linewidth(2*SCL)

plt.tight_layout()
plt.savefig(cfg.IO["PLOTS"] + "hist_lum.png", dpi=300)

#############################################
# Plot Area Histogram
#############################################
plt.figure(figsize=(11, 7))
font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22*SCL}
plt.rc('font', **font)


if not MAX_AREA:
    MAX_AREA = np.amax(area)
area = area[area <= MAX_AREA]
area = area[area >= MIN_AREA]

plt.hist(area, color="dimgrey", bins=int(round((min(MAX_AREA, np.amax(area))-MIN_AREA)/1)))
mono = 80
plt.axvline(x=mono, alpha=0.5, color="red", linestyle="--", linewidth=3*SCL)
plt.axvline(x=2*mono, alpha=0.5, color="red", linestyle="--", linewidth=3*SCL)
plt.axvline(x=3*mono, alpha=0.5, color="red", linestyle="--", linewidth=3*SCL)
plt.axvline(x=4*mono, alpha=0.5, color="red", linestyle="--", linewidth=3*SCL)

plt.xlabel("Area in px")
plt.ylabel("Quantity")
plt.xlim(left=MIN_AREA, right=MAX_AREA)
ax = plt.gca()

ax.tick_params(width=2*SCL, length=7*SCL)
for axis in ['top', 'bottom', 'left', 'right']:
    ax.spines[axis].set_linewidth(2*SCL)

plt.tight_layout()
plt.savefig(cfg.IO["PLOTS"] + "hist_area.png", dpi=300)

#############################################
# Plot Mass (Lum) Histogram
#############################################
plt.clf()

plt.figure(figsize=(11, 7))
font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22*SCL}
plt.rc('font', **font)


if not MAX_MASS:
    MAX_MASS = np.amax(mass_lum)

mass_lum = mass_lum[mass_lum <= MAX_MASS]
mass_lum = mass_lum[mass_lum >= MIN_MASS]

plt.hist(mass_lum, color="dimgrey", bins=int(round((min(MAX_MASS, np.amax(mass_lum))-MIN_MASS)/1)))

plt.xlabel("Mass in Units of Monomer Mass")
plt.ylabel("Quantity")
plt.xlim(left=MIN_MASS, right=MAX_MASS)
plt.ylim(top=MAX_MASS_QUANT)
ax = plt.gca()

ax.tick_params(width=2*SCL, length=7*SCL)
for axis in ['top', 'bottom', 'left', 'right']:
    ax.spines[axis].set_linewidth(2*SCL)

plt.tight_layout()
plt.savefig(cfg.IO["PLOTS"] + "hist_mass_lum.png", dpi=300)

#############################################
# Plot Mass (Area) Histogram
#############################################
plt.clf()

plt.figure(figsize=(11, 7))
font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 22*SCL}
plt.rc('font', **font)


if not MAX_MASS:
    MAX_MASS = np.amax(mass_area)
mass_area = mass_area[mass_area <= MAX_MASS]
mass_area = mass_area[mass_area >= MIN_MASS]

plt.hist(mass_area, color="dimgrey", bins=int(round((min(MAX_MASS, np.amax(mass_area))-MIN_MASS)/1)))

plt.xlabel("Mass in Units of Monomer Mass")
plt.ylabel("Quantity")
plt.xlim(left=MIN_MASS, right=MAX_MASS)
plt.ylim(top=MAX_MASS_QUANT)
ax = plt.gca()

ax.tick_params(width=2*SCL, length=7*SCL)
for axis in ['top', 'bottom', 'left', 'right']:
    ax.spines[axis].set_linewidth(2*SCL)

plt.tight_layout()
plt.savefig(cfg.IO["PLOTS"] + "hist_mass_area.png", dpi=300)

# for i, ar in enumerate(area):
#     print(str(area[i]) + "\t" + str(mass_area[i]) + "\t" + str(mass_lum[i]))



