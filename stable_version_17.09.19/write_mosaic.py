import parcas as pca
import config as cfg
import os


def main(mosaic=None):
    if not mosaic:
        mosaic = pca.mosaic_from_xml(cfg.IO["MOSAIC_XML"])
    pca.mosaic_to_png(mosaic, cfg.IO["MOSAIC_FFC"], key="ffc", mode="L",
                      bg_color=0, pad=(0.02, 0.02), draw_outlines=True, outline_color=255, outline_width=2,
                      show_progress=True)


if __name__ == "__main__":
    if cfg.BOOL["COPY_CONFIG"]:
        from shutil import copyfile
        copyfile("config.py", cfg.IO["PROJECT"] + "config.py")

    main()

    if cfg.BOOL["PLAY_DONE_SOUND"] and os.path.exists("done_sound.mp3"):
        from playsound import playsound
        playsound("done_sound.mp3")



