import parcas as pca
import pandas as pd
import cv2
import numpy as np
import os
import config as cfg


def main(quantities=("luminance", "area", "mass_lum", "mass_area", "d2", "d3"), mosaic=None, to_xml=True):
    #############################################
    # Initialize
    #############################################
    if not mosaic:
        mosaic = pca.mosaic_from_xml(cfg.IO["MOSAIC_XML"])
    dim = mosaic.dim

    analyze1 = pca.contains_any(quantities, ("luminance", "mass_lum"))
    analyze2 = pca.contains_any(quantities, ("area", "mass_area", "d2", "d3"))

    if not os.path.exists(cfg.IO["FFC_PART"]) and analyze1:
        os.makedirs(cfg.IO["FFC_PART"])
    if not os.path.exists(cfg.IO["FFC_BW_ER_PART"]) and analyze2:
        os.makedirs(cfg.IO["FFC_BW_ER_PART"])

    if os.path.exists(cfg.IO["DF_AVRG"]):
        df = cv2.imread(cfg.IO["DF_AVRG"])
    else:
        df = pca.avrg_field_from_folder(cfg.IO["DF"])
    if os.path.exists(cfg.IO["FF_AVRG"]):
        ff = cv2.imread(cfg.IO["FF_AVRG"])
    else:
        ff = pca.avrg_field_from_folder(cfg.IO["FF"])

    #############################################
    # Get Mean Background
    #############################################
    mean_bg = 0
    # if analyze1:
    #     bgs = []
    #     for j in range(dim[1]):
    #         for i in range(dim[0]):
    #             print(str(j) + "\t" + str(i))
    #             ffc = mosaic[j][i].get_image("ffc")
    #             ffc = pca.cvt_bgr_to_gray(ffc)
    #             # ffc = cv2.cvtColor(ffc, cv2.COLOR_BGR2GRAY)
    #             part_area = 0
    #             for key in mosaic[j][i].parts.keys():
    #                 for p, part in enumerate(mosaic[j][i].parts[key]):
    #                     ffc = cv2.rectangle(ffc, part.rpos,
    #                                         (part.rpos[0] + part.static_shape[1], part.rpos[1] + part.static_shape[0]),
    #                                         (0, 0, 0), thickness=-1)
    #                     part_area += part.static_shape[0] * part.static_shape[1]
    #             bgs.append(np.sum(ffc)/(ffc.shape[:2][0] * ffc.shape[:2][1] - part_area))
    #     mean_bg = np.mean(bgs)
    #     print(mean_bg)
    #     print()

    mean_bg = 95.23163999725364
    #############################################
    # Cut Out Particles from FFC with Label Mask
    #############################################
    data = pd.DataFrame(columns=mosaic[0][0].parts["full"][0].collect_data().columns)
    for j in range(dim[1]):
        for i in range(dim[0]):
            print(str(j) + "\t" + str(i))
            ffc = ffc_bw = None
            if analyze1:
                ffc = mosaic[j][i].get_image("ffc")
                ffc = pca.cvt_bgr_to_gray(ffc)
                # ffc = cv2.cvtColor(ffc, cv2.COLOR_BGR2GRAY)
                ffc = 1. - ffc / mean_bg
                # ffc2 = ffc.copy()
                # idx = np.where(ffc2 < 0)
                # ffc2 = np.around(ffc2 * 255).astype(np.uint8)
                # ffc2 = np.stack((ffc2,)*3, axis=-1)
                # ffc2[idx] = [255, 0, 0]
                # print(ffc2[0][0])
                # pca.show(ffc2, 0.3)
                # cv2.imwrite("F:/Desktop/ffc2.png", ffc2)
                ffc[ffc < 0] = 0
                # pca.show(np.around(ffc*255).astype(np.uint8), 0.3)
                # cv2.imwrite("F:/Desktop/ffc.png", np.around(ffc*255).astype(np.uint8))
            if analyze2:
                ffc_bw = mosaic[j][i].get_image("ffc_bw", flags=0)
            labels = np.load(mosaic[j][i].path["label"])

            for p, part in enumerate(mosaic[j][i].parts["full"]):
                if analyze1:
                    part_img = pca.apply_label_mask(ffc, labels.copy(), part)
                    np.save(cfg.IO["FFC_PART"] + str(j) + "_" + str(i) + "_" + str(part.label) + ".npy", part_img)
                    # cv2.imwrite(cfg.IO["FFC_PART"] + str(j) + "_" + str(i) + "_" + str(part.label) + ".png", part_img)
                    mosaic[j][i].parts["full"][p].path["ffc"] = cfg.IO["FFC_PART"] + str(j) + "_" + str(i) \
                                                                + "_" + str(part.label) + ".npy"
                    mosaic[j][i].parts["full"][p].luminance = pca.measure_luminance(part_img)
                if analyze2:
                    part_img = pca.apply_label_mask(ffc_bw, labels.copy(), part)
                    part_img = pca.fill_holes(part_img)
                    part_img = cv2.erode(part_img, cfg.PROCESS["EROSION_KERNEL"], iterations=cfg.PROCESS["ERODE"])
                    cv2.imwrite(cfg.IO["FFC_BW_ER_PART"] + str(j) + "_" + str(i) + "_" + str(part.label) + ".png",
                                part_img)
                    mosaic[j][i].parts["full"][p].path["ffc_bw_er"] = cfg.IO["FFC_BW_ER_PART"] + str(j) + "_"\
                                                                      + str(i) + "_" + str(part.label) + ".png"
                    mosaic[j][i].parts["full"][p].area = np.count_nonzero(part_img)

                mosaic[j][i].parts["full"][p].update_mass(cfg.CALIB["MONO_LUMINANCE"], cfg.CALIB["MONO_AREA"])
                data = data.append(part.collect_data(), ignore_index=True)
    data.to_csv(cfg.IO["DATA"])
    if to_xml:
        print("Writing " + cfg.IO["MOSAIC_XML"])
        pca.mosaic_to_xml(mosaic, cfg.IO["MOSAIC_XML"])
    return mosaic


if __name__ == "__main__":
    if cfg.BOOL["COPY_CONFIG"]:
        from shutil import copyfile
        copyfile("config.py", cfg.IO["PROJECT"] + "config.py")

    main()

    if cfg.BOOL["PLAY_DONE_SOUND"] and os.path.exists("done_sound.mp3"):
        from playsound import playsound
        playsound("done_sound.mp3")

