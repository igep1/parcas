import parcas as pca
import cv2
import numpy as np
import os
import config as cfg
from sys import exit
from time import sleep


def main(to_xml=True):
    #############################################
    # Check Contaminant File
    #############################################
    if not cfg.IO["CON_XML"]:
        print("No contaminants file given.")
    elif not os.path.exists(cfg.IO["CON_XML"]):
        print("No contaminants file found.")

    #############################################
    # Initialize
    #############################################
    if os.path.exists(cfg.IO["DF_AVRG"]):
        df = cv2.imread(cfg.IO["DF_AVRG"])
    else:
        df = pca.avrg_field_from_folder(cfg.IO["DF"])
    if os.path.exists(cfg.IO["FF_AVRG"]):
        ff = cv2.imread(cfg.IO["FF_AVRG"])
    else:
        ff = pca.avrg_field_from_folder(cfg.IO["FF"])

    shape = ff.shape[:2]
    if cfg.BOOL["DISPLAY_OVERLAP"]:
        ovd = pca.OverlapDisplay(shape)
    else:
        ovd = None

    con = pca.parse_con_data(cfg.IO["CON_XML"])
    files = os.listdir(cfg.IO["RGB"])
    stack = []
    dim = cfg.MOSAIC["DIM"]
    end_on = cfg.MOSAIC["END_ON"]
    if end_on < 0 or end_on > dim[0] * dim[1]:
        end_on = dim[0] * dim[1]
    if not cfg.MOSAIC["ROI_FAIL_COUNT"]:
        roi_fail_count = [10, 10]
    if not cfg.MOSAIC["ROI_UNIQUE"]:
        roi_unique = dict()
    for i in range(end_on):
        stack.append(pca.Slice(path={"rgb": cfg.IO["RGB"] + files[i]}, static_shape=shape, con=con[i]))

    #############################################
    # Load First Image
    #############################################
    img1 = stack[0].get_image(key="rgb")

    '''
    img1 = cv2.subtract(ff, img1)
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    if cfg.MOSAIC["MODE"] == 1:
        _, thr_img1 = cv2.threshold(img1, cfg.PROCESS["THRESH_ADAPTIVE"], 255, cv2.THRESH_BINARY)
        img1 = thr_img1 & img1
        del thr_img1
        img1 = cv2.adaptiveThreshold(img1, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 3, 0)
        img1 = pca.fill_1px_gaps(img1)
        img1 = pca.fill_porous(img1)
    else:
        _, img1 = cv2.threshold(img1, cfg.PROCESS["THRESH_NORMAL"], 255, cv2.THRESH_BINARY)
    if cfg.PROCESS["ERODE_ADAPTIVE"] > 0:
        img1 = cv2.erode(img1, cfg.PROCESS["EROSION_KERNEL"].copy(), iterations=cfg.PROCESS["ERODE_ADAPTIVE"])
    '''

    if not os.path.exists(cfg.IO["FFC"]):
        os.makedirs(cfg.IO["FFC"])
    else:
        for file in os.listdir(cfg.IO["FFC"]):
            os.remove(cfg.IO["FFC"] + file)
    if not os.path.exists(cfg.IO["FFC_BW"]):
        os.makedirs(cfg.IO["FFC_BW"])
    else:
        for file in os.listdir(cfg.IO["FFC_BW"]):
            os.remove(cfg.IO["FFC_BW"] + file)

    img1 = pca.ffc(img1, ff, df)
    np.save(cfg.IO["FFC"] + "0.npy", img1)
    # cv2.imwrite(cfg.IO["FFC"] + "0.png", img1)
    stack[0].path["ffc"] = cfg.IO["FFC"] + "0.npy"

    img1 = np.around(img1).astype(np.uint8)
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    _, img1 = cv2.threshold(img1, cfg.PROCESS["THRESH_FFC"], 255, cv2.THRESH_BINARY_INV)
    cv2.imwrite(cfg.IO["FFC_BW"] + "0.png", img1)
    stack[0].path["ffc_bw"] = cfg.IO["FFC_BW"] + "0.png"

    #############################################
    # Start Compare Loop
    #############################################
    d = 0
    if cfg.MOSAIC["START_POS"] == pca.TOP_LEFT:
        d = 1
    elif cfg.MOSAIC["START_POS"] == pca.TOP_RIGHT:
        d = -1
    for f in range(1, len(stack)):

        #############################################
        # Load Next Image
        #############################################
        img2 = stack[f].get_image(key="rgb")

        '''
        img2 = cv2.subtract(ff, img2)
        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
        if cfg.MOSAIC["MODE"] == pca.MODE_ADAPTIVE:
            _, thr_img2 = cv2.threshold(img2, cfg.PROCESS["THRESH_ADAPTIVE"], 255, cv2.THRESH_BINARY)
            img2 = thr_img2 & img2
            del thr_img2
            img2 = cv2.adaptiveThreshold(img2, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 3, 0)
            img2 = pca.fill_1px_gaps(img2)
            img2 = pca.fill_porous(img2)
        else:
            _, img2 = cv2.threshold(img2, cfg.PROCESS["THRESH_NORMAL"], 255, cv2.THRESH_BINARY)
        if cfg.PROCESS["ERODE_ADAPTIVE"] > 0:
            img2 = cv2.erode(img2, cfg.PROCESS["EROSION_KERNEL"].copy(), iterations=cfg.PROCESS["ERODE_ADAPTIVE"])
        '''

        img2 = pca.ffc(img2, ff, df)
        np.save(cfg.IO["FFC"] + str(f) + ".npy", img2)
        # cv2.imwrite(cfg.IO["FFC"] + str(f) + ".png", img2)
        stack[f].path["ffc"] = cfg.IO["FFC"] + str(f) + ".npy"

        img2 = np.around(img2).astype(np.uint8)
        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
        _, img2 = cv2.threshold(img2, cfg.PROCESS["THRESH_FFC"], 255, cv2.THRESH_BINARY_INV)
        cv2.imwrite(cfg.IO["FFC_BW"] + str(f) + ".png", img2)
        stack[f].path["ffc_bw"] = cfg.IO["FFC_BW"] + str(f) + ".png"

        #############################################
        # Get Particles From New Image
        #############################################
        _, labels, stats, centroids = cv2.connectedComponentsWithStats(img2, 4, cv2.CV_32S)
        stats = stats[np.lexsort(stats.T[[4]])]
        stats = stats[:-1]

        #############################################
        # Get Relative Coordinates
        #############################################
        rpos = None
        rev = False
        n = cfg.MOSAIC["ROI_START_N"]
        fail_count = cfg.MOSAIC["ROI_FAIL_COUNT"]
        max_part = cfg.MOSAIC["ROI_MAX_PART"]
        if "i{}".format(f) in list(cfg.MOSAIC["ROI_UNIQUE"].keys()):
            unique = cfg.MOSAIC["ROI_UNIQUE"]["i{}".format(f)]
            n = unique[0]
            fail_count = unique[1]
            max_part = unique[2]

        roi1, roi2 = [], []
        while rpos is None and n < fail_count[1]:
            n += 1
            if f % dim[0] != 0:
                if d == -1:
                    roi1, roi2 = pca.get_rois(img2, stats, pca.RIGHT, cfg.MOSAIC["MIN_AREA"],
                                              cfg.MOSAIC["MIN_EDGE_DIST"], n, max_part=max_part, f=f)
                else:
                    roi1, roi2 = pca.get_rois(img2, stats, pca.LEFT, cfg.MOSAIC["MIN_AREA"],
                                              cfg.MOSAIC["MIN_EDGE_DIST"], n, max_part=max_part, f=f)
            else:
                roi1, roi2 = pca.get_rois(img2, stats, pca.UP, cfg.MOSAIC["MIN_AREA"],
                                          cfg.MOSAIC["MIN_EDGE_DIST"], n, max_part=max_part, f=f)
                rev = True
            rpos = pca.get_rpos(img1, roi1, roi2, fail_count[0])
        if rev:
            d *= -1
        if n == fail_count[1]:
            print("\nTemplate Search Unsuccessful for Image " + files[f] + " (" + str(f + 1) + ").")
            exit()
        stack[f].roi = (roi1, roi2)
        stack[f].rpos = rpos

        #############################################
        # Display Overlap
        #############################################
        if ovd:
            ovd.next(img1, img2, f, rpos=rpos)
            ovd.show()
        else:
            print(f)

        #############################################
        # End Compare Loop
        #############################################
        img1 = img2

    #############################################
    # Tidy Up
    #############################################
    # del background
    sleep(1)
    cv2.destroyAllWindows()
    # print(stack)

    #############################################
    # Create and Export Mosaic
    #############################################
    mosaic = pca.Mosaic(dim, stack, pca.TOP_RIGHT)
    if to_xml:
        print("Writing " + cfg.IO["MOSAIC_XML"])
        pca.mosaic_to_xml(mosaic, cfg.IO["MOSAIC_XML"])
    return mosaic


if __name__ == "__main__":
    if cfg.BOOL["COPY_CONFIG"]:
        from shutil import copyfile
        copyfile("config.py", cfg.IO["PROJECT"] + "config.py")

    main()

    if cfg.BOOL["PLAY_DONE_SOUND"] and os.path.exists("done_sound.mp3"):
        from playsound import playsound
        playsound("done_sound.mp3")

