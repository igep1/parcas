import config as cfg
import os
import build_mosaic
import find_particles
import analyze
import write_mosaic

if cfg.BOOL["COPY_CONFIG"]:
    from shutil import copyfile
    copyfile("config.py", cfg.IO["PROJECT"] + "config.py")

print("+++ Step 1: Building Mosaic +++")
mosaic = build_mosaic.main(to_xml=False)
print("\n+++ Step 2: Finding Particles +++")
mosaic = find_particles.main(mosaic=mosaic, to_xml=False)
print("\n+++ Step 3: Analyzing +++")
mosaic = analyze.main(mosaic=mosaic, to_xml=True)
print("\n+++ Step 4: Writing Mosaic +++")
write_mosaic.main(mosaic=mosaic)

if cfg.BOOL["PLAY_DONE_SOUND"] and os.path.exists("done_sound.mp3"):
    from playsound import playsound
    playsound("done_sound.mp3")
