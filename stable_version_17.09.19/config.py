from parcas import constants as const
import numpy as np

PROJECT_PATH = "./S2/B+1/"

IO = dict(
    PROJECT=PROJECT_PATH,
    RGB=PROJECT_PATH + "rgb/",
    FF=PROJECT_PATH + "ff.jpg",
    FF_AVRG=PROJECT_PATH + "ff.jpg",
    DF=PROJECT_PATH + "df/",
    DF_AVRG=PROJECT_PATH + "df.png",
    CON_XML=PROJECT_PATH + "contaminants.xml",
    DATA=PROJECT_PATH + "data.csv",
    MOSAIC_XML=PROJECT_PATH + "mosaic.xml",
    MOSAIC_FFC=PROJECT_PATH + "mosaic_ffc.png",
    FFC=PROJECT_PATH + "ffc/",
    FFC_MARK=PROJECT_PATH + "ffc_mark/",
    FFC_BW=PROJECT_PATH + "ffc_bw/",
    FFC_BW_DIL=PROJECT_PATH + "ffc_bw_dil/",
    FFC_BW_ER=PROJECT_PATH + "ffc_bw_er/",
    LABEL=PROJECT_PATH + "label/",
    FFC_PART=PROJECT_PATH + "ffc_part/",
    FFC_BW_ER_PART=PROJECT_PATH + "ffc_bw_er_part/",
    PLOTS=PROJECT_PATH + "plots/"
)

BOOL = dict(
    COPY_CONFIG=True,
    PLAY_DONE_SOUND=True,
    DISPLAY_OVERLAP=True
)

CALIB = dict(
    MONO_LUMINANCE=(10232, 406),
    MONO_AREA=(80, 0),
    PARTICLE_DIAMETER=(1.5, 0),
    INTEGRATED_HEIGHT=(8, 2),
    SCALE=(0.14851463592652753, 0.0003565724536046285)
)

MOSAIC = dict(
    DIM=(10, 12),
    START_POS=const.TOP_RIGHT,
    BACKTRACK=True,
    MODE=const.MODE_ADAPTIVE,
    MIN_AREA=50,
    MIN_EDGE_DIST=250,
    END_ON=-1,
    ROI_START_N=1,
    ROI_FAIL_COUNT=(10, 10),
    ROI_MAX_PART=20,
    ROI_UNIQUE=dict()
)

PROCESS = dict(
    # THRESH_NORMAL=30,
    # THRESH_ADAPTIVE=52,
    THRESH_FFC=65,
    ERODE=2,
    EROSION_KERNEL=np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], np.uint8),
    DILATE_FFC=1,
    DILATION_KERNEL=np.array([[0, 255, 0], [255, 255, 255], [0, 255, 0]], np.uint8)
)

# COLOR = dict(
#     CON=100,
#     EDGE_PART=100
# )

